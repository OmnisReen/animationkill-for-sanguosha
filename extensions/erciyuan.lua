module("extensions.erciyuan", package.seeall)
extension = sgs.Package("erciyuan")
------------------------------------------------------------------------武将登陆区
yj=sgs.General(extension, "yj", "Erciyuan", 9, true,true,true) --完全隐藏武将
itoumakoto=sgs.General(extension, "itoumakoto", "Erciyuan", 3, true,false) --伊藤誠
ayanami=sgs.General(extension,"ayanami","Erciyuan",3,false,false) --綾波レイ
keima=sgs.General(extension,"keima","Erciyuan",3,true,false) --桂馬
kiriko=sgs.General(extension,"kiriko","Erciyuan",4,false,false) --キリコ
odanobuna=sgs.General(extension,"odanobuna","Erciyuan",3,false,false) --織田信奈
yuuta=sgs.General(extension,"yuuta","Erciyuan",4,true,false) --勇太
tsukushi=sgs.General(extension,"tsukushi","Erciyuan",3,false,false) --筑紫
batora=sgs.General(extension,"batora","Erciyuan",3,true,true) --バトラ
mao_maoyu=sgs.General(extension,"mao_maoyu","Erciyuan",4,false,false) --まお
sheryl=sgs.General(extension,"sheryl","Erciyuan",3,false,false) --シェリル
aoitori=sgs.General(extension,"aoitori","Erciyuan",4,true,false) --葵‘トリ
kyouko=sgs.General(extension,"kyouko","Erciyuan",4,false,false) --きょうこ
diarmuid=sgs.General(extension,"diarmuid","Erciyuan",4,true,false) --迪卢木多
ikarishinji=sgs.General(extension,"ikarishinji","Erciyuan",3,true,false) --碇シンジ
runaria=sgs.General(extension,"runaria","Erciyuan",3,false,false) --ルナリア
redarcher=sgs.General(extension,"redarcher","Erciyuan",4,true,false) --红Archer
redo=sgs.General(extension,"redo","Erciyuan",3,true,false) --レド
fuwaaika=sgs.General(extension,"fuwaaika","Erciyuan",3,false,false) --不破愛花
slsty=sgs.General(extension,"slsty","Erciyuan",3,false,false) --塞蕾丝缇雅
rokushikimei=sgs.General(extension,"rokushikimei","Erciyuan",3,true,true) --六識命
bernkastel=sgs.General(extension,"bernkastel","Erciyuan",3,false,true) --贝伦卡斯泰露
hibiki=sgs.General(extension,"hibiki","Erciyuan",3,false,false) --立花響
kntsubasa=sgs.General(extension,"kntsubasa","Erciyuan",4,false,false) --風鳴翼
khntmiku=sgs.General(extension,"khntmiku","Erciyuan",3,false,false) --小日向未来
yukinechris=sgs.General(extension,"yukinechris","Erciyuan",3,false,false) --雪音クリス
sagara=sgs.General(extension,"sagara","Erciyuan",4,true,false) --相良良晴
toumakazusa=sgs.General(extension,"toumakazusa","Erciyuan",3,false,false) --冬馬和紗
naoeriki=sgs.General(extension,"naoeriki","Erciyuan",3,true,false) --直枝理樹
------------------------------------------------------------------------特殊代码区
do
    require  "lua.config"
	local config = config
	local kingdoms = config.kingdoms
	table.insert(kingdoms,"Erciyuan")
	config.color_de = "#FF77FF"
end

getmoesenlist = function(room, player, taipu) --OmnisReen --作用：萌战技通用、得出参战角色list
	local ResPlayers = sgs.SPlayerList()
	local targets = room:getAlivePlayers()
	local newdata = sgs.QVariant()
	newdata:setValue(player)
	ResPlayers:append(player)
	for _,p in sgs.qlist(room:getOtherPlayers(player)) do
		if p:getMark(taipu) > 0 then
			if p:askForSkillInvoke("moesenskill", newdata) then
				ResPlayers:append(p)
				targets:removeOne(p)
			end
		end
	end
	targets:removeOne(player)
	return ResPlayers,targets
end
player2serverplayer = function(room, player) --啦啦SLG (OTZ--ORZ--Orz) --作用：将currentplayer转换成serverplayer
	local players = room:getPlayers()
	for _,p in sgs.qlist(players) do
		if p:objectName() == player:objectName() then
			return p
		end
	end
end
qstring2serverplayer = function(room, qstring) --改编版本 --作用：将qstring类型转换成serverplayer
	local players = room:getPlayers()
	for _,p in sgs.qlist(players) do
		if p:objectName() == qstring then
			return p
		end
	end
end
------------------------------------------------------------------------武器技能区 By独孤安河（OTZ--ORZ--orz）
GuanchuanDummyCard = sgs.CreateSkillCard{
	name = "GuanchuanDummyCard",
}
GuanchuanVS = sgs.CreateViewAsSkill{
	name = "#GuanchuanVS",
	n = 2,
	view_filter = function(self, selected, to_select)
        return #select < 2
	end,
	view_as = function(self, cards)
		return GuanchuanDummyCard:clone()
	end,
	enabled_at_play = function(self, target)
		return false
	end,
	enabled_at_response = function(self, target, pattern)
		return pattern == "@Axe"
	end
}
GuanchuanSkill = sgs.CreateTriggerSkill{
	name = "GuanchuanSkill",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.SlashMissed},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local effect = data:toSlashEffect()
		local dest = effect.to
        if dest:isAlive() then
			local prompt = string.format("@axe:%s", dest:objectName())
			local card = room:askForCard(player, "@Axe", prompt, data, sgs.CardDiscarded)
			if card then
				room:setEmotion(dest, "weapon/axe")
				local msg = sgs.LogMessage()
				msg.type = "#AxeSkill"
				msg.from = player
				msg.to:append(dest)
				msg.arg = self:objectName()
				room:sendLog(msg)
				room:slashResult(effect, nil)
			end
		end
        return false
	end
}
ZhuishaSkill = sgs.CreateTriggerSkill{
	name = "ZhuishaSkill",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.SlashMissed},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local effect = data:toSlashEffect()
		local dest = effect.to
        if dest:isAlive() then
			if effect.from:canSlash(dest, nil, false) then
				local prompt = string.format("blade-slash:%s", dest:objectName())
				if room:askForUseSlashTo(player, dest, prompt) then
					room:setEmotion(player,"weapon/blade")
				end
			end
		end
        return false
	end,
	priority = -1
}
CixiongSkill = sgs.CreateTriggerSkill{
	name = "CixiongSkill",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.TargetConfirmed},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local use = data:toCardUse()
		local source = use.from
        if source:objectName() == player:objectName() then
			local targets = use.to
			for _,target in sgs.qlist(targets) do
				if source:isMale() ~= target:isMale() then
					if not target:isSexLess() then
						if use.card:isKindOf("Slash") then
							if source:askForSkillInvoke(self:objectName()) then
								room:setEmotion(source, "weapon/double_sword")
								local draw_card = false
								if target:isKongcheng() then
									draw_card = true
								else
									local prompt = string.format("double-sword-card:%s", source:getGeneralName())
									local card = room:askForCard(target, ".", prompt, sgs.QVariant(), sgs.CardDiscarded)
									if not card then
										draw_card = true
									end
								end
								if draw_card then
									source:drawCards(1)
								end
							end
						end
					end
				end
			end
		end
        return false
	end,
	can_trigger = function(self, target)
		if target then
			if target:isAlive() and target:hasSkill(self:objectName()) then
				return not target:isSexLess()
			end
		end
		return false
	end
}
HuoshanSkill = sgs.CreateTriggerSkill{
	name="HuoshanSkill",
	events={sgs.CardUsed},
	frequency=sgs.Skill_NotFrequent,
	on_trigger=function(self,event,player,data)
		local room=player:getRoom()
		if event==sgs.CardUsed then
			local use=data:toCardUse()
			local card=use.card
			if not card:isKindOf("Slash") or card:isKindOf("ThunderSlash")or card:isKindOf("FireSlash") then return false end
			if not room:askForSkillInvoke(player, self:objectName()) then return end
			local newslash=sgs.Sanguosha:cloneCard("fire_slash",card:getSuit(),card:getNumber())
			newslash:addSubcard(card:getId())
			newslash:setSkillName("HuoshanSkill")
			use.card=newslash
			room:useCard(use,true)
			return true
		end 
	end,
}
BaojiSkill = sgs.CreateTriggerSkill{
	name = "BaojiSkill",
	frequency = sgs.Skill_Compuslory,
	events = {sgs.DamageCaused},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local damage = data:toDamage()
		local slash = damage.card
		local dest = damage.to
		if slash then
			if slash:isKindOf("Slash") then
				if dest:isKongcheng() then
					if not damage.chain then
						if not damage.transfer then
							room:setEmotion(dest, "weapon/guding_blade")
							local msg = sgs.LogMessage()
							msg.type = "#GudingBladeEffect"
							msg.from = player
							msg.to:append(dest)
							local point = damage.damage
							msg.arg = string.format("%d", point)
							point = point + 1
							msg.arg2 = string.format("%d", point)
							room:sendLog(msg)
							damage.damage = point
							data:setValue(damage)
						end
					end
				end
			end
		end
        return false
	end
}
ZhangbaSkill = sgs.CreateViewAsSkill{
	name = "ZhangbaSkill",
	n = 2,
	view_filter = function(self, selected, to_select)
		if #selected < 2 then
			return not to_select:isEquipped()
		end
		return false
	end,
	view_as = function(self, cards)
		if #cards == 2 then
			local cardA = cards[1]
			local cardB = cards[2]
			local suit = sgs.Card_NoSuit
			if cardA:isBlack() and cardB:isBlack() then
				suit = sgs.Card_Club
			end
			if cardA:isRed() and cardB:isRed() then
				suit = sgs.Card_Diamond
			end
			local slash = sgs.Sanguosha:cloneCard("slash", suit, 0)
			slash:setSkillName(self:objectName())
			slash:addSubcard(cardA)
			slash:addSubcard(cardB)
			return slash
		end
	end,
	enabled_at_play = function(self, player)
		return sgs.Slash_IsAvailable(player)
	end,
	enabled_at_response = function(self, player, pattern)
		return pattern == "slash"
	end
}
ShemaSkill = sgs.CreateTriggerSkill{
	name = "ShemaSkill",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.DamageCaused},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local damage = data:toDamage()
		local slash = damage.card
		local dest = damage.to
		local source = damage.from
        if slash then
			if slash:isKindOf("Slash") then
				if not damage.chain then
					if not damage.transfer then
						local DefHorse = dest:getDefensiveHorse() 
						local OffHorse = dest:getOffensiveHorse() 
						if DefHorse or OffHorse then
							if player:askForSkillInvoke(self:objectName(), data) then
								room:setEmotion(player, "weapon/kylin_bow")
								local horse_type
								if DefHorse then
									if OffHorse then
										horse_type = room:askForChoice(player, self:objectName(), "dhorse+ohorse")
									else
										horse_type = "dhorse"
									end
								else
									horse_type = "ohorse"
								end
								if horse_type == "dhorse" then
									room:throwCard(DefHorse, dest, source)
								elseif horse_type == "ohorse" then
									room:throwCard(OffHorse, dest, source)
								end
							end
						end
					end
				end
			end
		end
        return false
	end
}
HanbingSkill = sgs.CreateTriggerSkill{
	name = "HanbingSkill",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.DamageCaused},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local damage = data:toDamage()
		local slash = damage.card
		local dest = damage.to
		if slash then
			if slash:isKindOf("Slash") then
				if not dest:isNude() then
					if not damage.chain then
						if not damage.transfer then
							if player:askForSkillInvoke("IceSword", data) then
								room:setEmotion(player, "weapon/ice_sword")
								local id = room:askForCardChosen(player, dest, "he", "ice_sword")
								local card = sgs.Sanguosha:getCard(id)
								room:throwCard(card, dest, player)
								if not dest:isNude() then
									id = room:askForCardChosen(player, dest, "he", "ice_sword")
									card = sgs.Sanguosha:getCard(id)
									room:throwCard(card, dest, player)
								end
								return true
							end
						end
					end
				end
			end
		end
		return false
	end
}
HuajiSkill = sgs.CreateTargetModSkill{
	name = "HuajiSkill",
	pattern = "Slash",
	extra_target_func = function(self, from, card)
		if from:isLastHandCard(card) then
			return 2
		else
			return 0
		end
	end,
}
------------------------------------------------------------------------技能引用区 By DGAH.Github （OTZ--ORZ--orz）
ak_Xiuluo = sgs.CreateTriggerSkill{ --修罗
	name = "ak_Xiuluo",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.EventPhaseStart},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local once_success = false
		repeat
			once_success = false
			if not player:askForSkillInvoke(self:objectName()) then return false end
			local card_id = room:askForCardChosen(player, player, "j", self:objectName())
			local card = sgs.Sanguosha:getCard(card_id)
			local suit_str = card:getSuitString()
			local pattern = string.format(".|%s|.|.|.",suit_str)
			if room:askForCard(player, pattern, "@ak_Xiuluoprompt", data, sgs.CardDiscarded) then
				room:throwCard(card, nil, player)
				once_success = true
			end
		until (not (player:getCards("j"):length() ~= 0 and once_success) )
		return false
	end,
	can_trigger = function(self, target)
		if target then
			if target:isAlive() and target:hasSkill(self:objectName()) then
				if target:getPhase() == sgs.Player_Start then
					if not target:isKongcheng() then
						local ja = target:getJudgingArea()
						return ja:length() > 0
					end
				end
			end
		end
		return false
	end
}
ak_Paoxiao = sgs.CreateTargetModSkill{ --咆哮
	name = "ak_Paoxiao",
	pattern = "Slash",
	residue_func = function(self, player)
		if player:hasSkill(self:objectName()) then
			return 1000
		end
	end,
}
ak_Wushuang = sgs.CreateTriggerSkill{ --无双
	name = "ak_Wushuang", 
	frequency = sgs.Skill_Compulsory, 
	events = {sgs.TargetConfirmed, sgs.SlashProceed},
	on_trigger = function(self, event, player, data) 
		local room = player:getRoom()
		if event == sgs.TargetConfirmed then
			local use = data:toCardUse()
			local card = use.card
			if card:isKindOf("Slash") then
				if use.from:objectName() == player:objectName() then
					room:setCardFlag(card, "WushuangInvke")
				end
			elseif card:isKindOf("Duel") then
				room:setCardFlag(card, "WushuangInvke")
			end
		elseif event == sgs.SlashProceed then
			local effect = data:toSlashEffect()
			local dest = effect.to
			if effect.slash:hasFlag("WushuangInvke") then
				local slasher = player:objectName()
				local hint = string.format("@wushuang-jink-1:%s", slasher)
				local first_jink = room:askForCard(dest, "jink", hint, sgs.QVariant(), sgs.CardUsed, player)
				local second_jink = nil
				if first_jink then
					hint = string.format("@wushuang-jink-2:%s", slasher)
					second_jink = room:askForCard(dest, "jink", hint, sgs.QVariant(), sgs.CardUsed, player)
				end
				local jink = nil
				if first_jink and second_jink then
					jink = sgs.Sanguosha:cloneCard("Jink", sgs.Card_NoSuit, 0)
					jink:addSubcard(first_jink)
					jink:addSubcard(second_jink)
				end
				room:slashResult(effect, jink)
			end
			return true
		end
		return false
	end
}
ak_Kuanggu = sgs.CreateTriggerSkill{ --狂骨
	name = "ak_Kuanggu",
	frequency = sgs.Skill_Compulsory, 
	events = {sgs.Damage, sgs.PreDamageDone},
	on_trigger = function(self, event, player, data) 
		local damage = data:toDamage()
		local room = player:getRoom()
		if (event == sgs.PreDamageDone) and damage.from and damage.from:hasSkill(self:objectName()) and damage.from:isAlive() then
			local weiyan = damage.from
			weiyan:setTag("invokeak_Kuanggu", sgs.QVariant((weiyan:distanceTo(damage.to) <= 1)))
		elseif (event == sgs.Damage) and player:hasSkill(self:objectName()) and player:isAlive() then
			local invoke = player:getTag("invokeak_Kuanggu"):toBool()
			player:setTag("invokeak_Kuanggu", sgs.QVariant(false))
			if invoke and player:isWounded() then
				local recover = sgs.RecoverStruct()
				recover.who = player
				recover.recover = damage.damage
				room:recover(player, recover)
			end
		end
		return false
	end, 
	can_trigger = function(self, target)
		return target
	end
}
------------------------------------------------------------------------技能区
--------------------------------------------------------------人渣@itoumakoto
renzha = sgs.CreateTriggerSkill{
	name = "renzha", 
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.Damaged},  
	on_trigger = function(self, event, player, data) 
		local room = player:getRoom()
			local damage = data:toDamage()
			local x = damage.damage
			for i = 0, x-1, 1 do
				room:drawCards(player,2,self:objectName())
				local ida = room:askForCardChosen(player,player,"h",self:objectName())
				player:addToPile("zha",ida)
					if room:askForSkillInvoke(player, self:objectName()) then
					    room:broadcastSkillInvoke("renzha")
						player:turnOver()
						room:drawCards(player,1,self:objectName())
					end
			end
	end,
}
--------------------------------------------------------------好船@itoumakoto
haochuantimes = sgs.CreateTriggerSkill{
	name = "#haochuantimes", 
	frequency = sgs.Skill_Compulsory, 
	events = {sgs.TurnStart},  
	on_trigger = function(self, event, player, data) 
		local room = player:getRoom()
		if player:hasFlag("haochuan_used") then
		room:setPlayerFlag(player,"-haochuan_used")
		end
	end
}
ak_RenzhaCard = sgs.CreateSkillCard{
	name = "ak_RenzhaCard", 
	target_fixed = true, 
	will_throw = true, 
	on_use = function(self, room, source, targets)
		local zha = source:getPile("zha")
		room:fillAG(zha,source)
		local idb = room:askForAG(source,zha,false,"ak_Renzha")
		room:throwCard(idb,source)
		room:clearAG(source)
		room:broadcastSkillInvoke("ak_Renzha")
		room:doLightbox("ak_Renzha$", 2000)
		local zrecover = sgs.RecoverStruct()
		zrecover.recover = (source:getPile("zha")):length() - 1
		zrecover.who = source
		room:recover(source,zrecover)
		room:setPlayerFlag(source,"haochuan_used")
		source:turnOver()
		local players = room:getOtherPlayers(source)
		for _,p in sgs.qlist(players) do
			if p:isAlive() then
				room:cardEffect(self, source, p)
			end
		end
	end,
	on_effect = function(self, effect)
		local dest = effect.to
		local room = dest:getRoom()
		local players = room:getOtherPlayers(dest)
		local nearest = 1000
		local distance_list = sgs.IntList()
		for _,player in sgs.qlist(players) do
			local dist = dest:distanceTo(player)
			distance_list:append(dist)
			if dist < nearest then
				nearest = dist
			end
		end
		local ak_nwu_targets = sgs.SPlayerList()
		local count = distance_list:length()
		for i=0, count, 1 do
			local dist = distance_list:at(i)
			if dist == nearest then
				local player = players:at(i)
				if dest:canSlash(player) then
					ak_nwu_targets:append(player)
				end
			end
		end
		if ak_nwu_targets:length() > 0 then
			if not room:askForUseSlashTo(dest, ak_nwu_targets, "@ak_nwu-slash") then
				room:loseHp(dest)
			end
		else
			room:loseHp(dest)
		end
	end
}
ak_Renzha = sgs.CreateViewAsSkill{
	name = "ak_Renzha", 
	n = 0, 
	view_as = function(self, cards) 
		return ak_RenzhaCard:clone()
	end, 
	enabled_at_play = function(self, player)
		local zhazi = player:getPile("zha")
		return (zhazi:length() > 0) and not player:hasFlag("haochuan_used")
	end
}
--------------------------------------------------------------微笑@ayanami
weixiao = sgs.CreateTriggerSkill{
	name = "weixiao", 
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.EventPhaseStart},  
	on_trigger = function(self, event, player, data)
		local phase=player:getPhase()
		local room=player:getRoom()
		local handnum=player:getHandcardNum()
		if handnum > 1 then
		if phase == sgs.Player_Finish then
			if room:askForSkillInvoke(player, self:objectName(),data) then
				local ida = room:askForCardChosen(player,player,"h","weixiao")
				local carda = sgs.Sanguosha:getCard(ida)
				local anum = carda:getNumber()
				room:throwCard(ida,nil)
				local idb = room:askForCardChosen(player,player,"h","weixiao_second")
				local cardb = sgs.Sanguosha:getCard(idb)
				local bnum = cardb:getNumber()
				room:throwCard(idb,nil)		
				local choice=room:askForChoice(player,self:objectName(),"a+b")
					local list = room:getAlivePlayers()
					if choice == "a" then
						local n = math.min(anum,bnum)
						local dest1 = room:askForPlayerChosen(player,list,"weixiao")
						local count = n / 2	
						room:drawCards(dest1,count,"weixiao")
						room:broadcastSkillInvoke("weixiao")
						room:doLightbox("weixiao$", 2000)
					end
					if choice == "b" then
						local n = math.max(anum,bnum)
						local dest2 = room:askForPlayerChosen(player,list,"weixiao")
						local count = (n+1) / 2
						room:askForDiscard(dest2, "weixiao", count, count, false, true)
						room:broadcastSkillInvoke("weixiao")
						room:doLightbox("weixiao$", 2000)
					end
			end
		end
		end
	end
}	
--------------------------------------------------------------女神@ayanami
nvshen = sgs.CreateMaxCardsSkill{
	name = "nvshen",
	extra_func = function(self,target)
		if target:hasSkill("nvshen") then
			local hanumax=target:getMaxHp()
			return hanumax
		end
	end
}
--------------------------------------------------------------神知@keima
ak_Shenzhi = sgs.CreateTriggerSkill{
	name = "ak_Shenzhi",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.EventPhaseStart,sgs.CardsMoveOneTime},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.EventPhaseStart then
			if player:getPhase() == sgs.Player_Start then
				local keima = room:findPlayerBySkillName(self:objectName())
				if keima and (not keima:isNude()) and (keima:objectName() ~= player:objectName()) then
					local card = room:askForCard(keima,".",prompt,data,self:objectName())
					if not card then return end
					room:setPlayerProperty(keima,"shenzhitype",sgs.QVariant(card:getType()))
					card:setFlags("shenzhidiscard")
				end
			elseif player:getPhase() == sgs.Player_NotActive then
				local keima = room:findPlayerBySkillName(self:objectName())
				room:setPlayerProperty(keima,"shenzhitype",sgs.QVariant())
			end
		elseif event == sgs.CardsMoveOneTime then
			local move = data:toMoveOneTime()
			if move.to_place ~= sgs.Player_DiscardPile then return end
			local keima = room:findPlayerBySkillName(self:objectName())
			if player:objectName() ~= keima:objectName() then return end
			for _,id in sgs.qlist(move.card_ids) do
				if sgs.Sanguosha:getCard(id):hasFlag("shenzhitype") then return end
				if sgs.Sanguosha:getCard(id):getType() == keima:property("shenzhitype"):toString() then
					local current = room:getCurrent()
					local choice
					if current and (not current:isKongcheng()) then
						choice = room:askForChoice(keima,self:objectName(),"seeandchoose+drawone")
					else
						choice = "drawone"
					end
					if choice == "seeandchoose" then
						local list = current:handCards()
						room:fillAG(list,keima)
						room:broadcastSkillInvoke("ak_Shenzhi",1)
						local card_id = room:askForAG(keima,list,true,self:objectName())
						room:obtainCard(keima,card_id,false)
						room:clearAG(keima)
					elseif choice == "drawone" then
						room:drawCards(keima,1)
					end
				end
			end
		end
	end,
	can_trigger = function(self, player)
		return player
	end
}
--------------------------------------------------------------攻略@keima
ak_Gonglve = sgs.CreateTriggerSkill{
	name = "ak_Gonglve" ,
	events = {sgs.Damage,sgs.Damaged} ,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local damage = data:toDamage()
		local target = nil
		if event == sgs.Damage then
			target = damage.to
		else
			target = damage.from
		end
		if not target or target:objectName() == player:objectName() then return false end
		local players = sgs.SPlayerList()
		players:append(player)
		players:append(target)
		room:sortByActionOrder(players)
		for i = 1, damage.damage, 1 do
			if not target:isAlive() or not player:isAlive() then return false end
			local value = sgs.QVariant()
			value:setValue(target)
			if room:askForSkillInvoke(player,self:objectName(),value) then
				room:drawCards(players,1,self:objectName())
			end
		end
	end
}
--------------------------------------------------------------连斩@kiriko
ak_ak47 = sgs.CreateTriggerSkill{
	name = "ak_ak47",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.TargetConfirming},
	on_trigger = function(self,event,player,data)
		local use = data:toCardUse()
		local room = player:getRoom()
		local kiriko = room:findPlayerBySkillName(self:objectName())
		local source = use.from
		local targets = use.to
		local target = targets:first()
		local card = use.card
		if source:objectName() == kiriko:objectName() then
			if card:isKindOf("Slash") and card:isBlack() then
				if targets:length() == 1 then
				if not room:askForSkillInvoke(kiriko,self:objectName(),data) then return end
					local x = kiriko:getLostHp()
					room:drawCards(kiriko,x,self:objectName())
					for i = 1 , x , 1 do
						targets:append(target)
					end
					use.from = source
					use.to = targets
					use.card = card
					data:setValue(use)
				end
			end
		end
	end,
	can_trigger = function(self,target)
		return target 
	end,
}
--------------------------------------------------------------光剑@kirito
ak_Guangjian = sgs.CreateTriggerSkill{
	name = "#ak_Guangjian",
	frequency = sgs.Skill_Frequent,
	events = {sgs.EventPhaseStart},
	view_as_skill = ak_GuangjianVS,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if player:getPhase() == sgs.Player_Play then
			if not room:askForSkillInvoke(player,self:objectName(),data) then return end
			local judge = sgs.JudgeStruct()
			judge.reason = self:objectName()
			judge.who = player
			room:judge(judge)
			local jc = judge.card
			if jc:getSuit() ~= nil then
				room:setPlayerProperty(player,"guangjiansuit",sgs.QVariant(jc:getSuitString()))
			end
		end
	end,
}
ak_GuangjianVS = sgs.CreateViewAsSkill{
	name = "ak_GuangjianVS",
	n = 1,
	view_filter = function(self, selected, to_select)
		if to_select:getSuitString() ~= sgs.Self:property("guangjiansuit"):toString() then return false end
		local weapon = sgs.Self:getWeapon()
		if (sgs.Sanguosha:getCurrentCardUseReason() == sgs.CardUseStruct_CARD_USE_REASON_PLAY) and sgs.Self:getWeapon() 
				and (to_select:getEffectiveId() == sgs.Self:getWeapon():getId()) and to_select:isKindOf("Crossbow") then
			return sgs.Self:canSlashWithoutCrossbow()
		else
			return true
		end
	end,
	view_as = function(self, cards)
		if #cards == 1 then
			local card = cards[1]
			local slash = sgs.Sanguosha:cloneCard("slash", card:getSuit(), card:getNumber()) 
			slash:addSubcard(card:getId())
			slash:setSkillName(self:objectName())
			return slash
		end
	end,
	enabled_at_play = function(self, player)
		return sgs.Slash_IsAvailable(player)
	end, 
}
--------------------------------------------------------------赤鬼@odanobuna
ak_Chigui=sgs.CreateTriggerSkill{
	name = "ak_Chigui",  
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.EventPhaseStart},  
	on_trigger = function(self, event, player, data) 
		if player:getPhase() == sgs.Player_Finish then
			local room = player:getRoom()
			local others = room:getOtherPlayers(player)
			for _,p in sgs.qlist(others) do
				local weapon = p:getWeapon()
				if weapon then
					if room:askForSkillInvoke(player,weapon:objectName()) then
						room:broadcastSkillInvoke("ak_Chigui")
						room:loseHp(player)
						player:obtainCard(weapon)
						room:drawCards(player,1,self:objectName())
					end
				end
			end
		end			
		return false	 
	end
}
--------------------------------------------------------------布武@odanobuna
ak_Buwu = sgs.CreateTriggerSkill{
	name = "ak_Buwu", 
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.Damage},  
	on_trigger = function(self, event, player, data)
		local damage = data:toDamage()
		local dest = damage.to
		if not dest:isDead() then
			local room = player:getRoom()
			if room:askForSkillInvoke(player, self:objectName(), data) then
				local hp = dest:getHp()
				local x = math.min(5, hp - 1)
				dest:drawCards(x)
				dest:turnOver()
				room:broadcastSkillInvoke("ak_Buwu")
				room:doLightbox("ak_Buwu$", 1000)
			end
		end
		return false
	end
}
--------------------------------------------------------------天魔@odanobuna
ak_Tianmo = sgs.CreateTriggerSkill{
	name = "#ak_Tianmo",  
	frequency = sgs.Skill_Frequent, 
	events = {sgs.SlashMissed},  
	on_trigger = function(self, event, player, data) 
		local effect = data:toSlashEffect()
		local room = player:getRoom()
		player:gainMark("@tianmo",1)
		return false
	end
}
ak_TianmoDefense = sgs.CreateTriggerSkill{
	name = "ak_TianmoDefense",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.DamageDone,sgs.PreHpLost},
	on_trigger = function(self,event,player,data)
		local tm = player:getMark("@tianmo")
	if tm > 0 then
		local room = player:getRoom()
		if room:askForSkillInvoke(player,self:objectName(),data) then
			player:loseMark("@tianmo")
			local msg = sgs.LogMessage()
			msg.type = "#TianmoDefense"
			msg.from = player
			room:sendLog(msg)
			room:broadcastSkillInvoke("ak_TianmoDefense")
			return true
		end
	end
	end,
	priority = 8
}
--------------------------------------------------------------妄想@yuuta
ak_Wangxiang = sgs.CreateViewAsSkill{
	name = "ak_Wangxiang",
	n = 1,
	view_filter = function(self, selected, to_select)
		local str = sgs.Self:property("wangxiang"):toString()
		if not to_select:isEquipped() then 
			return not string.find(str,to_select:getSuitString())
		end
	end,
	view_as = function(self, cards)
		if #cards == 0 then
			return nil
		elseif #cards == 1 then
			local card = cards[1]
			local ex_nihilo = sgs.Sanguosha:cloneCard("ex_nihilo", card:getSuit(), card:getNumber())
			local wangxiangtb = {}
			table.insert(wangxiangtb,card:getSuitString())
			sgs.Self:setProperty("wangxiang", sgs.QVariant(table.concat(wangxiangtb, "+")))
			ex_nihilo:addSubcard(card:getId())
			ex_nihilo:setSkillName(self:objectName())
			return ex_nihilo
		end
	end,
	enabled_at_play = function(self, player)
		return player:getLostHp() > player:getHandcardNum()
	end,
}
--------------------------------------------------------------黑焰@yuuta
ak_BlackflameCard = sgs.CreateSkillCard{
	name = "ak_BlackflameCard",
	target_fixed = false,
	will_throw = true,
	filter = function(self, targets, to_select)
		if #targets == 0 then
			return to_select:objectName() ~= sgs.Self:objectName()
		end
		return false
	end,
	on_use = function(self, room, source, targets)
		local target = targets[1]
		local room = target:getRoom()
		local flame = sgs.DamageStruct()
		flame.from = source
		flame.to = target
		flame.damage = 1
		flame.nature = sgs.DamageStruct_Fire
		room:loseHp(source)
		room:broadcastSkillInvoke("ak_Blackflame")
		room:doLightbox("ak_Blackflame$", 1000)
		room:damage(flame)
	end
}
ak_Blackflame = sgs.CreateViewAsSkill{
	name = "ak_Blackflame",
	n = 0,
	view_as = function(self, cards)
		local card = ak_BlackflameCard:clone()
		card:setSkillName(self:objectName())
		return card 
	end,
	enabled_at_play = function(self, player)
		return not player:hasUsed("#ak_BlackflameCard")
	end
}
--------------------------------------------------------------钢躯@tsukushi
ak_Gqset = sgs.CreateTriggerSkill{
	name = "ak_Gqset",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.EventPhaseStart},
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		if player:getPhase() == sgs.Player_Finish then
			if not player:isKongcheng() then
				local gang = player:getPile("gang")
				local gangnum = gang:length()
				if gangnum == 0 then
					if room:askForSkillInvoke(player, self:objectName()) then
						local idn = room:askForCardChosen(player,player,"h",self:objectName())
						player:addToPile("gang",idn)
					end
				end
			end
		end
		if player:getPhase() == sgs.Player_Start then
			local gang = player:getPile("gang")
			local gangnum = gang:length()
			local idx = -1
			if gangnum > 0 then
				idx = gang:first()
				room:throwCard(idx,player)
				gangnum = gang:length()
			end
		end
	end,
}
ak_Gqef = sgs.CreateTriggerSkill{
	name = "#ak_Gqef",
	frequency = sgs.Skill_Compulsory, 
	events = {sgs.CardEffect,sgs.CardEffected},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
			local gang = player:getPile("gang")
			local gangnum = gang:length()
				if gangnum > 0  then
						local effect = data:toCardEffect()
						local target = effect.to
						local card = effect.card
						local card_type = card:getTypeId()
						local gangcardid = gang:first()
						local gangcard = sgs.Sanguosha:getCard(gangcardid)
						local gangcard_type = gangcard:getTypeId()
						if target:objectName() == player:objectName() then
							if card_type == gangcard_type then
							room:broadcastSkillInvoke("ak_Gqset")
							room:doLightbox("ak_Gqset$", 800)
							return true
							end
						end
			end
	end,
}
--------------------------------------------------------------调教@tsukushi
ak_TiaojiaoCard = sgs.CreateSkillCard{
	name = "ak_TiaojiaoCard", 
	target_fixed = false,
	will_throw = true, 
	filter = function(self, targets, to_select)
		if #targets == 0 then
			return to_select:objectName() ~= sgs.Self:objectName()
		end
		return false
	end,
	on_effect = function(self, effect)
		local source = effect.from
		local dest = effect.to
		local room = source:getRoom()
		local list = room:getAlivePlayers() 
		local dead = room:askForPlayerChosen(source,list,self:objectName())
		local prompt = string.format("@TiaojiaoSlash:%s:%s:%s",source:getGeneralName(),dest:getGeneralName(),dead:getGeneralName())
		room:broadcastSkillInvoke("ak_Tiaojiao")
		if not room:askForUseSlashTo(dest, dead, prompt) then
			if not dest:isNude() then
				local cardn = room:askForCardChosen(source, dest, "hej", self:objectName())
				room:obtainCard(source,cardn,false)
			end
		end
	end
}
ak_Tiaojiao = sgs.CreateViewAsSkill{
	name = "ak_Tiaojiao",
	n = 0, 
	view_as = function(self, cards) 
		return ak_TiaojiaoCard:clone()
	end, 
	enabled_at_play = function(self, player)
		return not player:hasUsed("#ak_TiaojiaoCard")
	end
}
--------------------------------------------------------------布棋@Batora
ak_Buqi = sgs.CreateTriggerSkill{
	name = "ak_Buqi", 
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.GameStart,sgs.Damaged},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local getcard = false
		local targets = room:getOtherPlayers(player)
		local givingcard = false
		local yourcardnum = 0
		for _,p in sgs.qlist(targets) do
			if not p:isKongcheng() then
				getcard = true
				break
			end
		end
		if getcard then
				if player:askForSkillInvoke(self:objectName()) then
					local targets = room:getOtherPlayers(player)
					for _,p in sgs.qlist(targets) do
						if not p:isKongcheng() then
							local yourcardnum = p:getHandcardNum()
							room:setPlayerMark(p,"chest",yourcardnum)
							local  allcard = p:wholeHandCards()
							room:obtainCard(player, allcard,false)
						end
					end
					getcard = false
					yourcardnum = 0
					local mycdnum = player:getHandcardNum()
					if mycdnum>0 then
						givingcard = true
					end
				end
			end
		if givingcard then
					local targets = room:getOtherPlayers(player)
					local targetnum = targets:length()
					local mycardnum = player:getHandcardNum()
					for _,p in sgs.qlist(targets) do
						if not player:isKongcheng() then
							local givingcardnum = p:getMark("chest")
							local prompt = string.format("@Buqigiving:%s:%s:%s",player:getGeneralName(),p:getGeneralName(),givingcardnum)
							local  givecard = room:askForExchange(player, self:objectName(), givingcardnum, false, prompt, false)
							room:obtainCard(p,givecard,false)
							room:setPlayerMark(p,"chest",0)
						end
					end
				givingcard = false
			end	
	end,
	priority = -2
}
--------------------------------------------------------------博学@mao_maoyu
ak_BoxueCard = sgs.CreateSkillCard{
	name = "ak_BoxueCard", 
	target_fixed = false,
	will_throw = true, 
	filter = function(self,targets, to_select)
	local num = sgs.Self:aliveCount()
		return #targets <= num
	end,
	on_use = function(self, room,source,targets)
		local num = #targets
		local alivenum = room:alivePlayerCount()
		local cards = room:getNCards(alivenum)
		local drawx = (num+1) / 2 
		local canexchange = false
		local urexcard
		source:drawCards(drawx)
		room:broadcastSkillInvoke("ak_Boxue")
		room:doLightbox("ak_Boxue$", 1200)
		for _,cs in sgs.qlist(cards) do
			local reason = sgs.CardMoveReason(sgs.CardMoveReason_S_REASON_SHOW,"","","","")
			room:moveCardTo(sgs.Sanguosha:getCard(cs), nil, sgs.Player_PlaceTable, reason, true)
		end
		for _,target in ipairs(targets) do
			room:fillAG(cards)
			local iwantcard = room:askForAG(target,cards,false,self:objectName())
			room:getThread():delay(1000)
			room:clearAG()
			room:clearAG(target)
				if not target:isNude() then
					canexchange = true
					urexcard = room:askForCardChosen(target,target,"he",self:objectName())
				end
			cards:removeOne(iwantcard)
			room:obtainCard(target,iwantcard,true)
				if canexchange then
					cards:append(urexcard)
					local reason = sgs.CardMoveReason(sgs.CardMoveReason_S_REASON_SHOW,"","","","")
					room:moveCardTo(sgs.Sanguosha:getCard(urexcard),nil, sgs.Player_PlaceTable, reason, true)
				end
			room:fillAG(cards)
			room:getThread():delay(1000)
			room:clearAG()
			canexchange = false
		end
		local choice=room:askForChoice(source,self:objectName(),"throw+gx")
		if choice =="throw" then
			for _,c in sgs.qlist(cards) do
			room:throwCard(c,nil,nil)
			end
		end
		if choice =="gx" then
			room:askForGuanxing(source,cards,true)
		end
	end,
}
ak_Boxue = sgs.CreateViewAsSkill{
	name = "ak_Boxue",
	n = 0, 
	view_as = function(self, cards) 
		return ak_BoxueCard:clone()
	end, 
	enabled_at_play = function(self, player)
		return not player:hasUsed("#ak_BoxueCard")
	end
}
--------------------------------------------------------------妖精@sheryl
ak_Yaojing = sgs.CreateViewAsSkill{
	name = "ak_Yaojing",
	n = 1,
	view_filter = function(self, selected, to_select)
		return true
	end,
	view_as = function(self, cards)
		if #cards == 0 then
			return nil
		elseif #cards == 1 then
			local card = cards[1]
			local god_salvation = sgs.Sanguosha:cloneCard("god_salvation", card:getSuit(), card:getNumber()) 
			god_salvation:addSubcard(card:getId())
			god_salvation:setSkillName(self:objectName())
			return god_salvation
		end
	end,
	enabled_at_play = function(self, player)
		return not player:hasUsed("#ak_Yaojing")
	end,
}
ak_YaojingSound =sgs.CreateTriggerSkill{
	name = "#ak_YaojingSound", 
	frequency = sgs.Skill_Frequent, 
	events = {sgs.CardUsed},
	on_trigger = function(self, event, player, data)
	local use = data:toCardUse()
	local room = player:getRoom()
	if use.card:getSkillName() == "ak_Yaojing" then
		room:broadcastSkillInvoke("ak_Yaojing")
		room:doLightbox("ak_Yaojing$",1500)
	end
	end,
}
--------------------------------------------------------------共鸣@sheryl
ak_Gongming = sgs.CreateTriggerSkill{
	name = "ak_Gongming", 
	frequency = sgs.Skill_Frequent, 
	events = {sgs.HpRecover},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local selfplayer=room:findPlayerBySkillName(self:objectName())
		if event == sgs.HpRecover then 
			local turn = selfplayer:getPhase()
			if turn == sgs.Player_Start or (turn  == sgs.Player_Judge) or(turn  == sgs.Player_Draw) or(turn  == sgs.Player_Play) or(turn  == sgs.Player_Discard)or(turn  == sgs.Player_Finish) then
				local choice=room:askForChoice(selfplayer,self:objectName(),"youdraw+hedraws")
				if choice == "youdraw" then
					local x = selfplayer:getLostHp()
					selfplayer:drawCards(x+1)
				end
				if choice == "hedraws" then
					local x = selfplayer:getLostHp()
					player:drawCards(x+1)
				end
			room:broadcastSkillInvoke("ak_Gongming")
			end
		end
	end,
	can_trigger = function(self, target)
		return (target ~= nil)
	end,
}
--------------------------------------------------------------裸王@aoitori
ak_Luowang = sgs.CreateProhibitSkill{
	name = "ak_Luowang",
	is_prohibited = function(self, from, to, card)
		return to:hasSkill(self:objectName()) and to:isNude() and (card:isBlack() or card:isKindOf("Slash"))
	end
}
--------------------------------------------------------------泛象@aoitori
ak_FanxiangCard = sgs.CreateSkillCard{
	name = "ak_FanxiangCard" ,
	will_throw = false,
	handling_method = sgs.Card_MethodNone,
	filter = function(self, selected, to_select)
		return (#selected == 0) and (to_select:getMark("FanxiangTarget") == 0) and (to_select:objectName() ~= sgs.Self:objectName())
	end,
	on_use = function(self, room, source, targets)
		local target = targets[1]
		room:setPlayerMark(target,"FanxiangTarget",1)
		local can_gain = false
		if self:getSubcards():length() >= 2 then
			can_gain = true
			local recover = sgs.RecoverStruct()
			recover.who = source
			recover.recover = 1
			room:recover(source, recover)
		end
		room:obtainCard(target, self, false)
		if can_gain then
			target:gainAnExtraTurn()
		end
	end,
}

ak_FanxiangVS = sgs.CreateViewAsSkill{
	name = "ak_FanxiangVS",
	n = 10086,
	enabled_at_play = function(self,player)
		return not player:isNude()
	end,
	view_filter = function(self,selected,to_select)
		return true
	end,
	view_as = function(self, cards)
		if #cards == 0 then return end
		local card = ak_FanxiangCard:clone()
		for _,c in ipairs(cards) do
			card:addSubcard(c)
		end
		return card
	end,
}

ak_Fanxiang = sgs.CreateTriggerSkill{
	name = "#ak_Fanxiang",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.EventPhaseStart},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		for _,p in sgs.qlist(room:getAlivePlayers()) do
			room:setPlayerMark(p, "FanxiangTarget", 0)
		end
	end,
	can_trigger = function(self, target)
		if target then
			if target:isAlive() then
				if target:hasSkill(self:objectName()) then
					return target:getPhase() == sgs.Player_NotActive
				end
			end
		end
		return false
	end
}
--------------------------------------------------------------破魔@diarmuid
ak_Pomo = sgs.CreateTriggerSkill{
	name = "ak_Pomo",
	events = {sgs.TargetConfirmed, sgs.CardFinished},
	frequency = sgs.Skill_Compulsory,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.TargetConfirmed then
			local use = data:toCardUse()
			if use.from and use.from:hasSkill(self:objectName()) then
				if use.card:isKindOf("Slash") then 
					if use.from:objectName() == player:objectName() then
					room:setPlayerFlag(use.from, "PomoArmor")
					room:broadcastSkillInvoke("ak_Pomo")
						for _,p in sgs.qlist(use.to) do
							room:setPlayerMark(p, "Armor_Nullified", 1) 
						end
					end
				end
			end
			return false
		else
			local use = data:toCardUse()
			if use.card:isKindOf("Slash") and use.from:hasFlag("PomoArmor") then
				for _,p in sgs.qlist(use.to) do
					room:setPlayerMark(p, "Armor_Nullified", 0)
				end
			end
		end
	end,
}
--------------------------------------------------------------必灭@diarmuid
ak_Bimie = sgs.CreateTriggerSkill{
	name = "ak_Bimie",
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.Damage}, 
	on_trigger = function(self, event, player, data)
		local damage = data:toDamage()
		local target = damage.to
		local source = damage.from
		local slash = damage.card
		local room = player:getRoom()
		if slash then
			if slash:isKindOf("Slash") then
				if source:objectName() == player:objectName() then
					if target:getMark("@zhou") == 0 then
						if	target:isAlive() then
							if not room:askForSkillInvoke(player, self:objectName()) then return end
							room:broadcastSkillInvoke("ak_Bimie")
							room:doLightbox("ak_Bimie$", 1500)
							target:gainMark("@zhou",1)
						end
					end
				end
			end
		end
	end,
}
ak_BimieHprcvForbidden = sgs.CreateTriggerSkill{
	name = "#ak_BimieHprcvForbidden",
	frequency = sgs.Skill_Compulsory, 
	events = {sgs.PreHpRecover}, 
	on_trigger = function(self, event, player, data)
		if player:getMark("@zhou") >0 then
			return true
		end
	end,
	can_trigger=function(self,target)
		return target
	end,
}
ak_BimieLost = sgs.CreateTriggerSkill{
	name = "#ak_BimieLost",  
	frequency = sgs.Skill_Compulsory, 
	events = {sgs.Death},
	on_trigger = function(self, event, player, data) 
		local death = data:toDeath()
		local victim = death.who
		local room = player:getRoom()
		local list = room:getAlivePlayers()
		if victim:objectName() == player:objectName() then
			for _,p in sgs.qlist(list) do
				room:setPlayerMark(p, "@zhou", 0)
			end
		end
		return false
	end, 
	can_trigger = function(self, target)
		if target then
			return target:hasSkill(self:objectName())
		end
		return false
	end
}
--------------------------------------------------------------暴走@ikarishinji
ak_Baozou = sgs.CreateTriggerSkill{
	name = "ak_Baozou",
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.AskForPeachesDone,sgs.EventPhaseEnd},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.AskForPeachesDone then
				if not room:askForSkillInvoke(player, self:objectName()) then return end
				if not player:hasSkill("ak_Paoxiao") then
					room:acquireSkill(player, ak_Paoxiao)
				end
				if not player:hasSkill("ak_Kuangshi") then
					room:acquireSkill(player, ak_Kuangshi)
				end
				if not player:hasSkill("ak_Yinbao") then
					room:acquireSkill(player, ak_Yinbao)
				end
				if not player:hasSkill("ak_Kuanggu") then
					room:acquireSkill(player,ak_Kuanggu)
				end
				if not player:hasSkill("ak_Wushuang") then
					room:acquireSkill(player,ak_Wushuang)
				end
				if not player:hasSkill("ak_Xiuluo") then
					room:acquireSkill(player,ak_Xiuluo)
				end
				room:setPlayerFlag(player,"BaozouTurn")
				room:broadcastSkillInvoke("ak_Baozou")
				player:gainAnExtraTurn()
		elseif event == sgs.EventPhaseEnd then
			local phase = player:getPhase()
			if phase == sgs.Player_Finish then
				if player:hasSkill("ak_Paoxiao") then
					room:detachSkillFromPlayer(player, "ak_Paoxiao")
				end
				if player:hasSkill("ak_Kuangshi") then
					room:detachSkillFromPlayer(player,"ak_Kuangshi")
				end
				if player:hasSkill("ak_Yinbao") then
					room:detachSkillFromPlayer(player,"ak_Yinbao")
				end
				if player:hasSkill("ak_Kuanggu") then
					room:detachSkillFromPlayer(player,"ak_Kuanggu")
				end
				if player:hasSkill("ak_Wushuang") then
					room:detachSkillFromPlayer(player,"ak_Wushuang")
				end
				if player:hasSkill("ak_Xiuluo") then
					room:detachSkillFromPlayer(player,"ak_Xiuluo")
				end
			end
		end
	end,
}
--------------------------------------------------------------狂噬Sound@ikarishinji
ak_BaozouSound =sgs.CreateTriggerSkill{
	name = "#ak_BaozouSound", 
	frequency = sgs.Skill_Frequent, 
	events = {sgs.CardUsed},
	on_trigger = function(self, event, player, data)
	local use = data:toCardUse()
	local room = player:getRoom()
	if use.card:getSkillName() == "ak_Kuangshi" then
		room:broadcastSkillInvoke("ak_Kuangshi")
	end
	end,
}
--------------------------------------------------------------狂噬@ikarishinji
ak_Kuangshi=sgs.CreateFilterSkill{
	name="ak_Kuangshi",
	view_filter=function(self,to_select)
		return to_select:isKindOf("BasicCard") or to_select:isKindOf("TrickCard")
	end,
	view_as=function(self,card)
		local KScard
		if card:isKindOf("TrickCard") then
			KScard=sgs.Sanguosha:cloneCard("duel",card:getSuit(),card:getNumber())
		elseif card:isKindOf("BasicCard") then
			KScard=sgs.Sanguosha:cloneCard("slash",card:getSuit(),card:getNumber())
		end
		local acard=sgs.Sanguosha:getWrappedCard(card:getId())
		acard:takeOver(KScard)
		acard:setSkillName(self:objectName())
		return acard
	end,
}
--------------------------------------------------------------音爆@ikarishinji
ak_Yinbao = sgs.CreateDistanceSkill{
	name = "ak_Yinbao",
	correct_func = function(self, from, to)
		if from:hasSkill("ak_Yinbao") then
			return -1000
		end
	end,
}
--------------------------------------------------------------不死@ikarishinji
ak_BaozouDying = sgs.CreateTriggerSkill{
	name = "#ak_BaozouDying",
	frequency = sgs.Skill_Compulsory,
	events = {sgs.PostHpReduced},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if player:hasFlag("BaozouTurn") then
			if player:getHp() <= 0  then
				room:setPlayerProperty(player, "hp", sgs.QVariant(player:getHp()))
				return true
			end
		end
		return false
	end,
}
--------------------------------------------------------------心壁@ikarishinji
ak_Xinbi = sgs.CreateTriggerSkill{
	name = "ak_Xinbi",
	frequency = sgs.Skill_Compulsory,
	events = {sgs.EventPhaseStart,sgs.CardsMoveOneTime},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.EventPhaseStart then
			local phase = player:getPhase()
			if phase == sgs.Player_Finish then
				if player:hasFlag("BaozouTurn") then return end
					local choice
					local targets = sgs.SPlayerList()
					local all = room:getAlivePlayers()
					for _,p  in sgs.qlist(all) do
						if p:getEquips():length() > 0 then
							targets:append(p)
						end
					end
					if not targets:isEmpty() then
						choice = room:askForChoice(player, self:objectName(), "throwtableequip+loseonehp", data)
					else
						choice = "loseonehp"
					end
					if choice == "throwtableequip" then
						local target = room:askForPlayerChosen(player, targets, self:objectName())
						local id = room:askForCardChosen(player, target , "e", self:objectName())
						room:throwCard(id,target,player)
					elseif choice == "loseonehp" then
						room:loseHp(player, 1)
						room:broadcastSkillInvoke("ak_Xinbi")
					end
			end
		elseif event == sgs.CardsMoveOneTime then
			local move = data:toMoveOneTime()
			local dest = move.to
			local hand = move.to_place
			local reason = move.reason
			local count = move.card_ids:length()
			local current = room:getCurrent()
			if reason then
				if hand == sgs.Player_PlaceHand then
					if dest:objectName() == player:objectName() then
						if count > 2 then 
							if current then
								if current:isAlive() then
									if current:getPhase() ~= sgs.Player_NotActive then
										for i = 0 , (count-1)/2 , 1 do
											room:loseHp(player)
											current:drawCards(1)
										end
									end
								end
							end
						end
					end
				end
			end
			room:broadcastSkillInvoke("ak_Xinbi")
		end
	end,	
}
--------------------------------------------------------------投影@redarcher
ak_TouyingClear = sgs.CreateTriggerSkill{
	name = "#ak_TouyingClear",
	frequency = sgs.Skill_Compulsory, 
	events = {sgs.EventPhaseEnd},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.EventPhaseEnd then
			local phase = player:getPhase()
			if phase == sgs.Player_Finish then
				local lose = player:property("weaponskilltoget"):toString()
				if lose ~= "" then
					room:detachSkillFromPlayer(player,lose)
				end
			end
		end
	end,
}
ak_TouyingCard = sgs.CreateSkillCard{
	name = "ak_TouyingCard", 
	target_fixed = true,
	will_throw = true, 
	on_use = function(self, room,source,targets)
		local choices = {"QinggangSword","Axe","Blade","Crossbow","DoubleSword","Fan","Spear","KylinBow","GudingBlade","IceSword","Halberd",}
		local weaponname = room:askForChoice(source,"ak_TouyingVS",table.concat(choices, "+"))
		if weaponname == "QinggangSword" then
			if not source:hasSkill("ak_Pomo") then
				room:acquireSkill(source, "ak_Pomo")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("ak_Pomo"))
			end
		end
		if weaponname == "Axe" then
			if not source:hasSkill("GuanchuanSkill") then
				room:acquireSkill(source, "GuanchuanSkill")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("GuanchuanSkill"))
			end
		end
		if weaponname == "Blade" then
			if not source:hasSkill("ZhuishaSkill") then
				room:acquireSkill(source, "ZhuishaSkill")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("ZhuishaSkill"))
			end
		end
		if weaponname == "Crossbow" then
			if not source:hasSkill("paoxiao") then
				room:acquireSkill(source, "paoxiao")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("paoxiao"))
			end
		end
		if weaponname == "DoubleSword" then
			if not source:hasSkill("CixiongSkill") then
				room:acquireSkill(source, "CixiongSkill")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("CixiongSkill"))
			end
		end
		if weaponname == "Fan" then
			if not source:hasSkill("HuoshanSkill") then
				room:acquireSkill(source, "HuoshanSkill")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("HuoshanSkill"))
			end
		end
		if weaponname == "Spear" then
			if not source:hasSkill("ZhangbaSkill") then
				room:acquireSkill(source, "ZhangbaSkill")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("ZhangbaSkill"))
			end
		end
		if weaponname == "KylinBow" then
			if not source:hasSkill("ShemaSkill") then
				room:acquireSkill(source, "ShemaSkill")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("ShemaSkill"))
			end
		end
		if weaponname == "GudingBlade" then
			if not source:hasSkill("BaojiSkill") then
				room:acquireSkill(source, "BaojiSkill")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("BaojiSkill"))
			end
		end
		if weaponname == "IceSword" then
			if not source:hasSkill("HanbingSkill") then
				room:acquireSkill(source, "HanbingSkill")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("HanbingSkill"))
			end
		end
		if weaponname == "Halberd" then
			if not source:hasSkill("HuajiSkill") then
				room:acquireSkill(source, "HuajiSkill")
				room:setPlayerProperty(source, "weaponskilltoget",sgs.QVariant("HuajiSkill"))
			end
		end
		room:broadcastSkillInvoke("ak_Touying")
	end,
}
ak_TouyingVS = sgs.CreateViewAsSkill{
	name = "ak_TouyingVS",
	n = 0, 
	view_as = function(self, cards) 
		return ak_TouyingCard:clone()
	end, 
	enabled_at_play = function(self, player)
		local weapon = player:getWeapon()
		if weapon == nil then
			return not player:hasUsed("#ak_TouyingCard")
		end
		return false
	end
}
--------------------------------------------------------------崩坏@redarcher
ak_Gongqi = sgs.CreateViewAsSkill{
	name = "ak_Gongqi", 
	n = 1, 
	view_filter = function(self, selected, to_select)
		local weapon = sgs.Self:getWeapon()
		if weapon and to_select:objectName() == weapon:objectName() and to_select:objectName() == "Crossbow" then
			return sgs.Self:canSlashWithoutCrossbow()
		end
		return to_select:isKindOf("EquipCard")
	end, 
	view_as = function(self, cards) 
		if #cards == 1 then
			local card = cards[1]
			local suit = card:getSuit()
			local point = card:getNumber()
			local id = card:getId()
			local slash = sgs.Sanguosha:cloneCard("slash", suit, point)
			slash:addSubcard(id)
			slash:setSkillName(self:objectName())
			return slash
		end
	end, 
	enabled_at_play = function(self, player)
		return sgs.Slash_IsAvailable(player)
	end, 
	enabled_at_response = function(self, player, pattern)
		return pattern == "slash"
	end
}
ak_GongqiTargetMod = sgs.CreateTargetModSkill{
	name = "#ak_Gongqi-target",
	distance_limit_func = function(self, from, card)
        if from:hasSkill("ak_Gongqi") and card:getSkillName() == "ak_Gongqi" then
            return 1000
        else
            return 0
		end
	end
}
--------------------------------------------------------------剑咏@redarcher
ak_Jianyong = sgs.CreateTriggerSkill{
	name = "ak_Jianyong",
	frequency = sgs.Skill_Frequent, 
	events = {sgs.Damage,sgs.EventPhaseEnd},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.Damage then
			local phase = player:getPhase()
			if  phase ~= sgs.Player_NotActive then
				room:setPlayerMark(player,"yongdamage",1)
			end
		elseif event == sgs.EventPhaseEnd then
			local phase = player:getPhase()
			if phase == sgs.Player_Discard then
				if player:getMark("yongdamage") == 0 then
					if not player:askForSkillInvoke(self:objectName()) then return end
					room:drawCards(player, 2)
					if not player:isKongcheng() then
						local card_id = -1
						local handcards = player:handCards()
						if handcards:length() == 1 then
							room:getThread():delay(800)
							card_id = handcards:first()
						else
							card_id = room:askForCardChosen(player,player,"h",self:objectName())
						end
						player:addToPile("yong",card_id)
						room:broadcastSkillInvoke("ak_Jianyong")
					end
				end
			elseif phase == sgs.Player_Finish  then
				room:setPlayerMark(player,"yongdamage",0)
			end
		end
	end,
}
--------------------------------------------------------------剑制@redarcher
ak_Jianzhi = sgs.CreateTriggerSkill{
	name = "ak_Jianzhi",
	frequency = sgs.Skill_Wake,
	events = {sgs.EventPhaseStart},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		player:gainMark("@waked")
		room:changeMaxHpForAwakenSkill(player)
		room:doLightbox("ak_Jianzhi$", 2500)
		room:broadcastSkillInvoke("ak_Jianzhi")
		room:acquireSkill(player, "ak_Jianyu")
		room:acquireSkill(player, "ak_Chitian")
		local targets = room:getOtherPlayers(player)
		for _,t in sgs.qlist(targets) do
			if not t:isKongcheng() then  
				local id = room:askForCardChosen(player, t, "h", "ak_Jianzhi")
				player:addToPile("yong",id)
			end
		end
		player:gainAnExtraTurn()
	end,
	can_trigger = function(self, target)
		if target then
			if target:isAlive() and target:hasSkill(self:objectName()) then
				if target:getMark("@waked") == 0 then
					if target:getPhase() == sgs.Player_Finish then
						local yong = target:getPile("yong")
						return (yong:length() >= 3)
					end
				end
			end
		end
		return false
	end
}
--------------------------------------------------------------剑雨@redarcher
ak_JianyuCard = sgs.CreateSkillCard{
	name = "ak_JianyuCard", 
	target_fixed = true,
	will_throw = true, 
	on_use = function(self, room,source,targets)
		local alivenum = room:alivePlayerCount()
		local yong = source:getPile("yong")
		local cards = sgs.IntList()
		for i = 0, alivenum-1, 1 do
			yong = source:getPile("yong")
			if yong:length() > 0 then
				room:fillAG(yong,source)
				local card_id = room:askForAG(source, yong, false, "ak_Jianyu")
				room:clearAG(source)
				yong:removeOne(card_id)
				cards:append(card_id)
				local reason = sgs.CardMoveReason(sgs.CardMoveReason_S_REASON_USE,"","","","")
				room:moveCardTo(sgs.Sanguosha:getCard(card_id), nil, sgs.Player_DiscardPile, reason, true)
			end
		end
		if cards:length() >= alivenum then
			local use = sgs.CardUseStruct()
			local aa = sgs.Sanguosha:cloneCard("archery_attack",sgs.Card_NoSuit,0) 
			aa:setSkillName("ak_Jianyu")
			use.from = source
			use.card = aa
			room:useCard(use, false)
			room:broadcastSkillInvoke("ak_Jianyu")
		end
	end,
}
ak_Jianyu = sgs.CreateViewAsSkill{
	name = "ak_Jianyu", 
	n = 0, 
	view_as = function(self, cards) 
		return ak_JianyuCard:clone()
	end, 
	enabled_at_play = function(self, player)
		local yong = player:getPile("yong")
		local alivenum = sgs.Self:aliveCount()
		return yong:length() >= alivenum
	end
}
--------------------------------------------------------------炽天@redarcher
ak_Chitian = sgs.CreateTriggerSkill{
	name = "ak_Chitian",
	frequency = sgs.NotFrequent,
	events = {sgs.CardAsked},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local pattern = data:toStringList()[1]
		local yong = player:getPile("yong")
		if yong:length() > 0 then
			if (pattern == "jink") then
				if sgs.Sanguosha:getCurrentCardUseReason() == sgs.CardUseStruct_CARD_USE_REASON_RESPONSE_USE then
					if not room:askForSkillInvoke(player, self:objectName(), data) then return false end
						room:fillAG(yong,player)
						local jink = room:askForAG(player,yong,false,self:objectName())
						room:clearAG(player)
						if jink then
							local givejink = sgs.Sanguosha:cloneCard("jink", sgs.Card_NoSuit, 0)
							givejink:addSubcard(jink)
							givejink:setSkillName(self:objectName())
							room:throwCard(jink,player)
							room:provide(givejink)
							return true
						end
				end
			elseif (pattern == "slash") then
				if (sgs.Sanguosha:getCurrentCardUseReason() == sgs.CardUseStruct_CARD_USE_REASON_RESPONSE) or (sgs.Sanguosha:getCurrentCardUseReason() == sgs.CardUseStruct_CARD_USE_REASON_RESPONSE_USE) then
					if not room:askForSkillInvoke(player, self:objectName(), data) then return false end
						room:fillAG(yong,player)
						local slash = room:askForAG(player,yong,false,self:objectName())
						room:clearAG(player)
						if slash then
							local giveslash = sgs.Sanguosha:cloneCard("slash", sgs.Card_NoSuit, 0)
							giveslash:addSubcard(slash)
							giveslash:setSkillName(self:objectName())
							room:throwCard(slash,player)
							room:provide(giveslash)
							return true
						end
				end
			end
		end
		return false
	end,
	can_trigger = function(self, player)
		if player then
			return player:hasSkill(self:objectName())
		end
		return false
	end
}
--------------------------------------------------------------去死吧,铁皮混蛋！@redo
ak_RedoWake = sgs.CreateTriggerSkill{
	name = "ak_RedoWake",
	frequency = sgs.Skill_Wake,
	events = {sgs.EventPhaseStart},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		player:gainMark("@waked")
		player:loseAllMarks("@Chamber")
		local wakercv = sgs.RecoverStruct()
		wakercv.recover = 1
		wakercv.who = player
		room:recover(player,wakercv)
		local targets = room:getOtherPlayers(player)
		local dest = room:askForPlayerChosen(player,targets,self:objectName())
		local qbd = sgs.DamageStruct()
		qbd.from = player
		qbd.to = dest
		qbd.damage = 3
		qbd.nature = sgs.DamageStruct_Thunder
		room:broadcastSkillInvoke("ak_RedoWake")
		room:doLightbox("$ak_RedoWake", 3600)
		room:damage(qbd)
		if  player:hasSkill("ak_Gaoxiao") then
			room:detachSkillFromPlayer(player,"ak_Gaoxiao")
		end
		if player:hasSkill("ak_Gaokang") then
			room:detachSkillFromPlayer(player,"ak_Gaokang")
		end
		if player:hasSkill("ak_JiguangAsk") then
			room:detachSkillFromPlayer(player,"ak_JiguangAsk")
		end
	end,
	can_trigger = function(self, target)
		if target then
			if target:isAlive() and target:hasSkill(self:objectName()) then
				if target:getMark("@waked") == 0 then
					if target:getPhase() == sgs.Player_Play	then
						return target:getHp() == 1
					end
				end
			end
		end
		return false
	end
}
--------------------------------------------------------------钱伯@redo
ak_ChamberStart = sgs.CreateTriggerSkill{
	name = "#ak_ChamberStart",
	frequency = sgs.Skill_Compulsory,
	events = {sgs.DrawInitialCards},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		player:loseAllMarks("@Chamber")
		player:gainMark("@Chamber", 1)
		if not player:hasSkill("ak_Gaoxiao") then
			room:acquireSkill(player,"ak_Gaoxiao")
		end
		if not player:hasSkill("ak_Gaokang") then
			room:acquireSkill(player,"ak_Gaokang")
		end
		if not player:hasSkill("ak_JiguangAsk") then
			room:acquireSkill(player,"ak_JiguangAsk")
		end
		local count = data:toInt() - 2
		data:setValue(count)
	end
}
ak_ChamberMove = sgs.CreateTriggerSkill{
	name = "ak_ChamberMove",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.EventPhaseStart},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if player:getMark("@Chamber") == 0 then
			if not room:askForSkillInvoke(player, self:objectName(), data) then return false end
			player:gainMark("@Chamber", 1)
			if not player:hasSkill("ak_Gaoxiao") then
				room:acquireSkill(player,"ak_Gaoxiao")
			end
			if not player:hasSkill("ak_JiguangAsk") then
				room:acquireSkill(player,"ak_JiguangAsk")
			end
			if not player:hasSkill("ak_Gaokang") then
				room:acquireSkill(player,"ak_Gaokang")
			end
			room:broadcastSkillInvoke("ak_ChamberMove")
		elseif player:getMark("@Chamber") == 1 then
			if not room:askForSkillInvoke(player, self:objectName(), data) then return false end
			player:loseAllMarks("@Chamber")
			if player:hasSkill("ak_Gaoxiao") then
				room:detachSkillFromPlayer(player,"ak_Gaoxiao")
			end
			if player:hasSkill("ak_JiguangAsk") then
				room:detachSkillFromPlayer(player,"ak_JiguangAsk")
			end
			if player:hasSkill("ak_Gaokang") then
				room:detachSkillFromPlayer(player,"ak_Gaokang")
			end
			room:broadcastSkillInvoke("ak_ChamberMove")
		end
	end,
	can_trigger = function(self, target)
		if target then
			if target:isAlive() and target:hasSkill(self:objectName()) then
				if target:getMark("@waked") == 0 then
					if target:getPhase() == sgs.Player_Start then
						return true
					end
				end
			end
		end
		return false
	end
}
--------------------------------------------------------------高效@redo
ak_Gaoxiao = sgs.CreateTriggerSkill{
	name = "ak_Gaoxiao",
	frequency = sgs.Skill_Frequent,
	events = {sgs.DrawNCards},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if room:askForSkillInvoke(player, "ak_Gaoxiao", data) then
			local count = data:toInt() + 1
			room:broadcastSkillInvoke("ak_Gaoxiao")
			data:setValue(count)
		end
	end
}
--------------------------------------------------------------高抗@redo
ak_Gaokang = sgs.CreateTriggerSkill{
	name = "ak_Gaokang",  
	frequency = sgs.Skill_Compulsory, 
	events = {sgs.DamageInflicted},  
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local damage = data:toDamage()
		local source = damage.from
		if not player:isNude() then
			if damage.nature == sgs.DamageStruct_Normal then
				local id = room:askForCardChosen(player, player, "he", self:objectName())
				room:throwCard(id, nil)
				damage.damage = damage.damage - 1
				room:broadcastSkillInvoke("ak_Gaokang")
				data:setValue(damage)
			end
		end
		return false
	end,
}
--------------------------------------------------------------激光@redo
ak_JiguangMod = sgs.CreateTargetModSkill{
	name = "#ak_JiguangMod",
	pattern = "Slash",
	distance_limit_func = function(self, player)
		if player:hasSkill("ak_JiguangAsk") then
			return 1000
		end
	end,
}
ak_JiguangAsk = sgs.CreateTriggerSkill{
	name = "ak_JiguangAsk",
	frequency = sgs.Skill_Compulsory, 
	events = {sgs.SlashProceed}, 
	on_trigger = function(self, event, player, data) 
		local room = player:getRoom()
		local effect = data:toSlashEffect()
		local dest = effect.to
		local source = effect.from
		if source:hasSkill(self:objectName()) then
			local prompt = string.format("@jiguang:%s:%s", source:getGeneralName(),dest:getGeneralName())
			if room:askForCard(dest, "TrickCard,EquipCard|.|.", prompt, data, sgs.CardDiscarded) then
				return true
			else
				room:broadcastSkillInvoke("ak_JiguangAsk")
				room:slashResult(effect, nil)
				return true
			end
		end
		return false
	end,
}
--------------------------------------------------------------无杀@redo
ak_RedoNoSlash = sgs.CreateProhibitSkill{
	name = "#ak_RedoNoSlash",
	is_prohibited = function(self, from, to, card)
		if to:hasSkill(self:objectName()) then
			if to:getMark("@Chamber") == 0 then
				return card:isKindOf("Slash")
			end
		end
	end,
}
--------------------------------------------------------------文乐@runaria
ak_Wenle = sgs.CreateTriggerSkill{
	name = "ak_Wenle",
	events= {sgs.DrawNCards,sgs.EventPhaseEnd,sgs.EventPhaseStart},
	frequency = sgs.Skill_Compulsory,
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		local x = (room:alivePlayerCount()) / 2
		if event == sgs.DrawNCards then
			local count = data:toInt() + x
			room:broadcastSkillInvoke("ak_Wenle")
			data:setValue(count)
		elseif event == sgs.EventPhaseEnd then
			local phase = player:getPhase()
			if phase == sgs.Player_Draw then
				for i=0,x-1,1 do
					local id = room:askForCardChosen(player, player, "h", self:objectName())
					player:addToPile("si",id,false)
				end
			end
		elseif event == sgs.EventPhaseStart then
			local phase = player:getPhase()
			if phase == sgs.Player_Start then
				local si = player:getPile("si")
				local x = si:length()
				player:clearOnePrivatePile("si")
				player:drawCards(x)
				room:broadcastSkillInvoke("ak_Wenle")
			end
		end
	end,
}
--------------------------------------------------------------御空@runaria
ak_YukongCard = sgs.CreateSkillCard{
	name = "ak_YukongCard",
	target_fixed = true,
	will_throw = true,
	on_use = function(self, room, source, targets)
		local si = source:getPile("si")
		room:fillAG(si,source)
		local id = room:askForAG(source,si,false,"ak_Yukong")
		room:throwCard(id,source)
		room:broadcastSkillInvoke("ak_Yukong")
		room:clearAG(source)
	end,
}
ak_Yukong = sgs.CreateViewAsSkill{
	name = "ak_Yukong",
	n = 0,
	view_as = function(self, cards)
		local card = ak_YukongCard:clone()
		card:setSkillName(self:objectName())
		return card 
	end,
	enabled_at_play = function(self, player)
		local si = player:getPile("si")
		if si:length() > 0 then
			return not player:hasUsed("#ak_YukongCard")
		end
		return false
	end
}
ak_YukongMod = sgs.CreateDistanceSkill{
	name = "#ak_YukongMod",
	correct_func = function(self, from, to)
		if from:hasUsed("#ak_YukongCard") then
			return -1000
		end
	end,
}
--------------------------------------------------------------绮丝@runaria
ak_Qisi = sgs.CreateTriggerSkill{
	name = "ak_Qisi",
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.CardEffected},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local si = player:getPile("si")
		local sinum = si:length()
		if sinum > 0  then
			local effect = data:toCardEffect()
			local target = effect.to
			local source = effect.from
			local card = effect.card
			if card then
				if card:isKindOf("Slash") or (card:isNDTrick())  then
					if not card:isVirtualCard() then
						if (target:objectName() == player:objectName())  then
							if not room:askForSkillInvoke(player, self:objectName(), data) then return end
							room:fillAG(si,player)
							local id = room:askForAG(player,si,false,"ak_Qisi")
							local cardx = sgs.Sanguosha:getCard(id)
							local num = (cardx:getNumber()) / 2
							room:clearAG(player)
							if card then
								room:throwCard(id,player)
								local itsar = source:getAttackRange()
								if num > itsar then
									room:broadcastSkillInvoke("ak_Qisi")
									return true
								end
							end
						end
					end
				end
			end
		end
	end,
}
--------------------------------------------------------------破始@fuwaaika
ak_PoshiCard = sgs.CreateSkillCard{
	name = "ak_PoshiCard",
	target_fixed = true,
	will_throw = true,
	on_use = function(self, room, source, targets)
		::lab:: if source:isKongcheng() then return end
		local list = sgs.SPlayerList()
		for _,p in sgs.qlist(room:getOtherPlayers(source)) do
			if not p:isKongcheng() then
				list:append(p)
			end
		end
		if list:isEmpty() then return end
		local target = room:askForPlayerChosen(source,list,"ak_Poshi")
		room:broadcastSkillInvoke("ak_Poshi")
		local pindian = source:pindian(target, "ak_Poshi")
		room:loseHp(source,1)
		if not pindian then
			if source:hasFlag("PoshiSuccess") then return end
			goto lab
		end
		if pindian then
			local recover = sgs.RecoverStruct()
			recover.who = source
			room:recover(source, recover)
		end
	end
}
ak_Poshi = sgs.CreateViewAsSkill{
	name = "ak_Poshi",
	n = 0,
	view_as = function(self, cards)
		local card = ak_PoshiCard:clone()
		card:setSkillName(self:objectName())
		return card
	end,
	enabled_at_play = function(self, player)
		return not player:hasUsed("#ak_PoshiCard")
	end,
}
ak_PoshiTMS = sgs.CreateTargetModSkill{
	name = "#ak_PoshiTMS",
	pattern = "Slash",
	residue_func = function(self, player)
		if player:hasUsed("#ak_PoshiCard") and player:hasFlag("PoshiSuccess") then
			return 1000
		end
	end,
}
ak_PoshiTrig = sgs.CreateTriggerSkill{
	name = "#ak_PoshiTrig",
	events = {sgs.Pindian,sgs.TargetConfirmed, sgs.CardFinished},
	frequency = sgs.Skill_Compulsory,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.Pindian then
			local pindian = data:toPindian()
			if pindian.reason == "ak_Poshi" then
				if pindian.from_card:getNumber() > pindian.to_card:getNumber() then
					room:setFixedDistance(player,pindian.to,1)
					room:setPlayerFlag(player,"PoshiSuccess")
				end
			end
		end
		if player:hasUsed("#ak_PoshiCard") then
			if not player:hasFlag("PoshiSuccess") then return end
			if event == sgs.TargetConfirmed then
				local use = data:toCardUse()
				if use.from and use.from:hasSkill(self:objectName()) then
					if use.card:isKindOf("Slash") then 
						if use.from:objectName() == player:objectName() then
						room:setPlayerFlag(use.from, "PoshiArmor")
							for _,p in sgs.qlist(use.to) do
								room:setPlayerMark(p, "Armor_Nullified", 1) 
							end
						end
					end
				end
				return false
			else
				local use = data:toCardUse()
				if not use.card then return end
				if use.card:isKindOf("Slash") and use.from:hasFlag("PoshiArmor") then
					for _,p in sgs.qlist(use.to) do
						room:setPlayerMark(p, "Armor_Nullified", 0)
					end
				end
			end
		end
	end,
}
--------------------------------------------------------------连锁@fuwaaika
ak_Liansuo = sgs.CreateTriggerSkill{
	name = "ak_Liansuo",
	events = {sgs.CardsMoveOneTime,sgs.EventPhaseEnd},
	frequency = sgs.Skill_NotFrequent,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.CardsMoveOneTime then
			local move = data:toMoveOneTime()
			local source = move.from
			local topc = move.to_place
			local pp = room:getCurrent()
			local aika = room:findPlayerBySkillName(self:objectName())
			if source == nil then return end
			if aika == nil then return end
			if aika:objectName() == pp:objectName() then return end
			if aika:objectName() ~= player:objectName() then return end
			if source:objectName() == aika:objectName() then return end
			if bit32.band(move.reason.m_reason, sgs.CardMoveReason_S_MASK_BASIC_REASON) ~= sgs.CardMoveReason_S_REASON_DISCARD then return end
			if topc ~= sgs.Player_DiscardPile then return end
			for _,id in sgs.qlist(move.card_ids) do
				if aika:getMark("aikadraw") < 3 then
					if aika:askForSkillInvoke(self:objectName()) then
						aika:drawCards(1)
						room:broadcastSkillInvoke("ak_Liansuo")
						room:addPlayerMark(aika,"aikadraw",1)
					end
				end
			end
		elseif event == sgs.EventPhaseEnd then
			local phase = player:getPhase()
			if phase == sgs.Player_Finish then
				local aika = room:findPlayerBySkillName(self:objectName())
				if aika == nil then return end
				room:setPlayerMark(aika,"aikadraw",0)
			end
		end
	end,
	can_trigger = function(self, target)
		return target ~= nil
	end,
}
--------------------------------------------------------------因果@fuwaaika
ak_Yinguo = sgs.CreateTriggerSkill{
	name = "ak_Yinguo",
	events = {sgs.Death},
	frequency = sgs.Skill_Limited,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local males = sgs.SPlayerList()
		local targets = sgs.SPlayerList()
		for _, p in sgs.qlist(room:getAlivePlayers()) do
			if p:isMale() then
				males:append(p)
			end
		end
		local death = data:toDeath()
		local selfplayer = room:findPlayerBySkillName(self:objectName())
		if death.who:objectName() ~= selfplayer:objectName() then return end
		if males:isEmpty() then return end
		if not player:isKongcheng() then
			if room:askForSkillInvoke(player, self:objectName(), data) then
				room:broadcastSkillInvoke("ak_Yinguo")
				local ids = player:handCards()
				local prompt = string.format("@yinguo")
				local man = room:askForPlayerChosen(player,males,self:objectName(),prompt,true,true)
				room:setPlayerMark(man,"todraw",man:getHandcardNum())
				local prompt2 = string.format("@yinguo2")
				local man2 = room:askForPlayerChosen(player,males,self:objectName(),prompt2,true,true)
				room:setPlayerMark(man2,"todraw",man2:getHandcardNum())
				if man ~= nil then 
					targets:append(man)
				end
				if man2 ~= nil then
					if man2:objectName() ~= man:objectName() then
						targets:append(man2)
					end
				end
				if man:objectName() ~= man2:objectName() then
					while room:askForYiji(player, ids, self:objectName(), false, false, false, -1, targets) do end
				else
					local hc = player:wholeHandCards()
					room:obtainCard(man,hc,false)
				end
				if man2:objectName() == man:objectName() then
					man:drawCards(man:getHandcardNum()-man:getMark("todraw"))
				else
					man2:drawCards(man2:getHandcardNum()-man2:getMark("todraw"))
					man:drawCards(man:getHandcardNum()-man:getMark("todraw"))
				end
			end
		end
	end,
	can_trigger = function(self, target)
		if target then
			return target:hasSkill(self:objectName())
		end
		return false
	end,
	priority = 4,
}
--------------------------------------------------------------言惑@slsty
function takeYanhuoCard(source, player, id)
	local room = player:getRoom()
	local card = sgs.Sanguosha:getCard(id)
	local suit = card:getSuit()
	local reason = sgs.CardMoveReason(sgs.CardMoveReason_S_REASON_SHOW, source:objectName(), player:objectName(), "slyanhuo", "")
	room:moveCardTo(card, nil, sgs.Player_PlaceTable, reason, true)
	room:broadcastSkillInvoke("slyanhuo")
	if source:hasSkill("zhahu") then
		local log = sgs.LogMessage()
		log.type = "#TriggerSkill"
		log.from = source
		log.arg = "zhahu"
		room:sendLog(log)
		if suit == sgs.Card_Spade then
			local recov = sgs.RecoverStruct()
			recov.who = source
			recov.recover = 1
			room:recover(player, recov)
		elseif suit == sgs.Card_Heart then
			room:loseHp(player)
		elseif suit == sgs.Card_Club then
			player:drawCards(2)
		elseif suit == sgs.Card_Diamond and not player:isNude() then
			local choice = room:askForCardChosen(source, player, "he", "zhahu")
			room:throwCard(choice, player, source)
		end
	end
	room:throwCard(card, nil)
	room:getThread():delay()
end	
yanhuoAcquireCard = sgs.CreateSkillCard
{
	name = "yanhuoAcquireCard",
	filter = function(self, selected, to_select)
		return #selected == 0 and to_select:hasSkill("slyanhuo") and not to_select:getPile("confuse"):isEmpty() and to_select:objectName() ~= sgs.Self:objectName()
	end,
	on_effect = function(self, effect)
		local room = effect.from:getRoom()
		if not effect.to:hasSkill("slyanhuo") or effect.to:getPile("confuse"):isEmpty() then
			return
		end
		local id = effect.to:getPile("confuse"):last()
		takeYanhuoCard(effect.to, effect.from, id)
	end
}
yanhuoAcquire = sgs.CreateViewAsSkill
{
	name = "yanhuoAcquire", 
	n = 0,
	view_as = function(self, cards)
		return yanhuoAcquireCard:clone()
	end,
	enabled_at_play = function(self, player)
		local others = player:getSiblings()
		local can_use = false
		for _,p in sgs.qlist(others) do
			if p:hasSkill("slyanhuo") and not p:getPile("confuse"):isEmpty() then
				can_use = true
				break
			end
		end
		return can_use and not player:hasUsed("#yanhuoAcquireCard")
	end
}
if not sgs.Sanguosha:getSkill("yanhuoAcquire") then
	local skillList=sgs.SkillList()
	skillList:append(yanhuoAcquire)
	sgs.Sanguosha:addSkills(skillList)
end
yanhuoCard = sgs.CreateSkillCard
{
	name = "slyanhuoCard",
	target_fixed = true, 
	will_throw = false, 
	on_use = function(self, room, player, targets)
		player:addToPile("confuse", self, false)
		player:drawCards(1)
	end
}
yanhuovs = sgs.CreateViewAsSkill
{
	name = "slyanhuo", 
	n = 1,
	view_filter = function(self, selected, to_select)
		return not to_select:isEquipped()
	end,
	view_as = function(self, cards)
		if #cards < 1 then return nil end
		local card = yanhuoCard:clone()
		card:addSubcard(cards[1])
		return card
	end,
	enabled_at_play = function(self, player)
		return player:getPile("confuse"):length() < 4
	end
}
slyanhuo = sgs.CreateTriggerSkill
{
	name = "slyanhuo",
	view_as_skill = yanhuovs,
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.GameStart, sgs.EventAcquireSkill, sgs.Damaged},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.GameStart or (event == sgs.EventAcquireSkill and data:toString() == self:objectName()) then
			for _,p in sgs.qlist(room:getOtherPlayers(player)) do
				if not p:hasSkill("yanhuoAcquire") then
					room:attachSkillToPlayer(p, "yanhuoAcquire")
				end
			end
		elseif event == sgs.Damaged then
			local damage = data:toDamage()
			local card_ids = player:getPile("confuse")
			if not damage.from or damage.from:isDead() or card_ids:isEmpty() 
				or not room:askForSkillInvoke(player, self:objectName(), data) then				
				return false
			end
			room:fillAG(card_ids, player)
			local id = room:askForAG(player, card_ids, true, self:objectName())
			room:clearAG(player)
			takeYanhuoCard(player, damage.from, id)
		end
		return false
	end
}
--------------------------------------------------------------诈唬@slsty
zhahu = sgs.CreateTriggerSkill
{
	name = "zhahu",
	frequency = sgs.Skill_Compulsory,
	events = sgs.EventPhaseStart,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local card_ids = player:getPile("confuse")
		if player:getPhase() ~= sgs.Player_Start or card_ids:isEmpty() then
			return false
		end
		room:broadcastSkillInvoke("zhahu")
		for i=1,card_ids:length() do
			takeYanhuoCard(player, player, card_ids:at(card_ids:length()-i))
		end
		return false
	end
}
--------------------------------------------------------------心灵诱导@rokushikimei
ak_Heartlead = sgs.CreateTriggerSkill{
	name = "ak_Heartlead",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.TargetConfirming},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local selfplayer = room:findPlayerBySkillName(self:objectName())
		local use = data:toCardUse()
		local card = use.card
		local source = use.from
		local targets = use.to
		if targets:length() ~= 1 then return end
		if source:objectName() == selfplayer:objectName() then return end
		if card:isKindOf("BasicCard") or (card:isNDTrick()) then
			if not room:askForSkillInvoke(selfplayer, self:objectName(), data) then return end
			if selfplayer:isChained() then
				if not selfplayer:faceUp() then
					return end
			end
			if not selfplayer:faceUp() then
				if selfplayer:isChained() then
					selfplayer:turnOver()
				end
			end
			if selfplayer:isChained() then
				selfplayer:turnOver()
			else
				local choice = room:askForChoice(selfplayer, self:objectName(), "chained+turnoverself", data)
				if choice == "chained" then
					room:setPlayerProperty(selfplayer, "chained", sgs.QVariant(true))
				elseif choice == "turnoverself" then
					selfplayer:turnOver()
				end
			end
			local tosecplayers = room:getAlivePlayers()
			for _,t in sgs.qlist(targets) do
				tosecplayers:removeOne(t)
			end
			local emptylist = sgs.PlayerList()
			local newtargetstosec = sgs.SPlayerList()
			for _,p  in sgs.qlist(tosecplayers) do
				if card:targetFilter(emptylist, p, source) then
					if not source:isProhibited(p, card) then
						newtargetstosec:append(p)
					end
				end
			end
			local target = room:askForPlayerChosen(source, newtargetstosec, self:objectName())
			local confirmedtarget = sgs.SPlayerList()
			confirmedtarget:append(target)
			if target ~= nil then
				use.from = source
				use.to = confirmedtarget
				use.card = card
				data:setValue(use)
			end
		end
	end,
	can_trigger = function(self, target)
		return target
	end,
}
--------------------------------------------------------------藏位转移@rokushikimei
ak_PositionMove = sgs.CreateTriggerSkill{
	name = "ak_PositionMove",
	frequency = sgs.Skill_Compulsory,
	events = sgs.DamageForseen,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local damage = data:toDamage()
		if player:isChained() or (player:faceUp() == false) then
			if player:objectName() == damage.to:objectName() then
				if player:getCards("he"):length() < 2 then return end
				if player:getCards("he"):length() == 2 then
					player:throwAllHandCardsAndEquips()
				elseif player:getCards("he"):length() > 2 then
					room:askForDiscard(player, self:objectName(),2 , 2, false, true)
				end
				if player:isChained() then
					room:setPlayerProperty(player, "chained", sgs.QVariant(false))
				end
				if not player:faceUp() then
					player:turnOver()
				end
			end
			return true
		end
	end,
}
--------------------------------------------------------------语言牢笼@rokushikimei
ak_Spkprison = sgs.CreateTriggerSkill{
	name = "ak_Spkprison",
	frequency = sgs.Skill_Limited,
	events = {sgs.GameStart,sgs.EventPhaseStart},
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		if event == sgs.GameStart then
			local selfplayer = room:findPlayerBySkillName(self:objectName())
			if selfplayer:getMark("@Spkprison") == 0 then
				selfplayer:gainMark("@Spkprison")
			end
		elseif event == sgs.EventPhaseStart then
			if player:getPhase() ~= sgs.Player_Start then return end
			local selfplayer = room:findPlayerBySkillName(self:objectName())
			if selfplayer:getMark("@Spkprison") == 0 then return end
			if player:objectName() == selfplayer:objectName() then return end
			if player:hasSkill(self:objectName()) then return end
			if not room:askForSkillInvoke(selfplayer, self:objectName(), data) then return end
			selfplayer:loseMark("@Spkprison")
			player:drawCards(3)
			local prompt = string.format("@rokugive:%s:%s:%s",player:getGeneralName(),selfplayer:getGeneralName(),self:objectName())
			local excards = room:askForExchange(player,self:objectName(),4,true,prompt,false)
			selfplayer:obtainCard(excards,false)
			return false
		end
	end,
	can_trigger = function(self, target)
		return target
	end,
}
--------------------------------------------------------------奇迹宣言@bernkastel
qiji = sgs.CreateTriggerSkill
{
	name = "qiji",
	events = sgs.Dying,
	frequency = sgs.Skill_NotFrequent,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local dying = data:toDying()
		if not dying.damage or not dying.damage.from or dying.damage.from:objectName() == dying.who:objectName()
			or dying.who:getMark("@miracle") > 0
			or not room:askForSkillInvoke(player, self:objectName(), data) then
			return false
		end
		dying.who:gainMark("@miracle")
		room:broadcastSkillInvoke("qiji")
		local judge = sgs.JudgeStruct()
		judge.negative = true
		judge.play_animation = false
		judge.who = dying.who
		judge.pattern = "."
		judge.good = false
		judge.reason = self:objectName()
		judge.time_consuming = true
		room:judge(judge)
		local tjudge = sgs.JudgeStruct()
		tjudge.negative = false
		tjudge.play_animation = true
		tjudge.who = dying.damage.from
		tjudge.pattern = ".|.|"..judge.card:getNumber()
		tjudge.good = true
		tjudge.reason = self:objectName()
		tjudge.time_consuming = true
		room:judge(tjudge)
		if tjudge:isGood() then
			local recov = sgs.RecoverStruct()
			recov.who = player
			recov.recover = dying.who:getMaxHp() - dying.who:property("hp"):toInt()
			room:recover(dying.who, recov)
			room:loseHp(dying.damage.from, math.max(dying.damage.from:property("hp"):toInt(), 0))
		end
		return false
	end
}
--------------------------------------------------------------碎片筛选@bernkastel
suipian = sgs.CreateTriggerSkill
{
	name = "suipian",
	events = sgs.AskForRetrial,
	frequency = sgs.Skill_NotFrequent,
	priority = 3,
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local judge = data:toJudge()
		local invoked = false
		while room:askForSkillInvoke(player, self:objectName(), data) do
			room:broadcastSkillInvoke("suipian")
			invoked = true
			local card = sgs.Sanguosha:getCard(room:drawCard())
			room:retrial(card, player, judge, self:objectName())
		end
		return invoked
	end
}
--------------------------------------------------------------圣枪@hibiki
ak_Gungnir = sgs.CreateTriggerSkill{
	name = "ak_Gungnir",
	events = {sgs.SlashProceed,sgs.ConfirmDamage,sgs.Damaged,sgs.EventPhaseEnd},
	frequency = sgs.Skill_Compulsory,
	on_trigger = function(self, event ,player, data)
		local room = player:getRoom()
		if event == sgs.SlashProceed then
			local effect = data:toSlashEffect()
			local source = effect.from
			if source:hasSkill(self:objectName()) then
				room:slashResult(effect, nil)
				room:broadcastSkillInvoke("ak_Gungnir",math.random(1,2))
				return true
			end
		elseif event == sgs.ConfirmDamage then
			local damage = data:toDamage()
			if damage.from:hasSkill(self:objectName()) then
				if damage.to:getHp() > damage.from:getHp() then
					damage.damage = damage.damage + 1
					data:setValue(damage)
				elseif damage.to:getHp() <= damage.from:getHp() then
					local rcv = sgs.RecoverStruct()
					rcv.recover = 1
					rcv.who = damage.from
					room:recover(damage.from,rcv)
				end
			end
		elseif event == sgs.Damage then
			if damage.from:hasSkill(self:objectName()) then
				room:setPlayerMark(damage.from,"GungnirSlash",damage.from:getMark("GungnirSlash")+1)
			end
		elseif event == sgs.EventPhaseStart then
			local phase = player:getPhase()
			if phase == sgs.Player_Finish then
				if player:hasSkill(self:objectName()) then
					if player:getMark("GungnirSlash") == 0 then return end
					room:broadcastSkillInvoke("ak_Gungnir",3)
					room:askForDiscard(player,self:objectName(),player:getMark("GungnirSlash"),player:getMark("GungnirSlash"),false,true)
					room:setPlayerMark(player,"GungnirSlash",0)
				end
			end
		end
	end,
}
--------------------------------------------------------------同步@hibiki
ak_Synchrogazer = sgs.CreateViewAsSkill{
	name = "ak_Synchrogazer",
	n = 0,
	view_as = function(self, cards) 
		return ak_SynchrogazerCard:clone()
	end, 
	enabled_at_play = function(self, player)
		return not player:hasFlag("SucSyn")
	end
}
ak_SynchrogazerCard = sgs.CreateSkillCard{
	name = "ak_SynchrogazerCard",
	target_fixed = true, 
	will_throw = true, 	
	on_use = function(self, room, source, targets)
		room:broadcastSkillInvoke("ak_Synchrogazer",1)
		ResPlayers,targets = getmoesenlist(room,source,"@zessho")
		if ResPlayers:length() > 3 or (targets:length() < 1) or (ResPlayers:length() == 0) then return end
		room:setPlayerFlag(source,"SucSyn")
		local target = room:askForPlayerChosen(source,targets,"ak_Synchrogazer_Target")
		local useslash = false
		room:broadcastSkillInvoke("ak_Synchrogazer",2)
		::lab::	for _, p in sgs.qlist(ResPlayers) do
			if not target:isAlive() then break end
			local prompt = string.format("@SynSlash:%s:%s",source:objectName(),target:objectName())
			if room:askForUseSlashTo(p,target,prompt,false,true,false) then
				useslash = true
				local drawp = room:askForPlayerChosen(source,ResPlayers,"ak_Synchrogazer_Friend")
				drawp:drawCards(1)
			else
				useslash = false
				break
			end
		end
		if useslash then 
			goto lab
		end
	end
}
--------------------------------------------------------------绝唱@战姬绝唱——系列人物
ak_Zessho = sgs.CreateTriggerSkill{
	name = "#ak_Zessho",
	events = {sgs.GameStart},
	frequency = sgs.Skill_Compulsory,
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		room:addPlayerMark(player,"@zessho",1)
	end,
	priority = 3
}
--------------------------------------------------------------苍闪@kntsubasa
ak_Cangshan = sgs.CreateViewAsSkill{
	name = "ak_Cangshan" ,
	n = 1 ,
	view_filter = function(self, selected, to_select)
		local weapon = sgs.Self:getWeapon()
		if (sgs.Sanguosha:getCurrentCardUseReason() == sgs.CardUseStruct_CARD_USE_REASON_PLAY) and sgs.Self:getWeapon() 
				and (to_select:getEffectiveId() == sgs.Self:getWeapon():getId()) and to_select:isKindOf("Crossbow") then
			return sgs.Self:canSlashWithoutCrossbow()
		else
			return to_select:getTypeId() == sgs.Card_Equip
		end
	end ,
	view_as = function(self, cards)
		if #cards ~= 1 then return nil end
		local originalCard = cards[1]
		local pattern = sgs.Sanguosha:getCurrentCardUsePattern()
		local reason = sgs.Sanguosha:getCurrentCardUseReason()
		if reason == sgs.CardUseStruct_CARD_USE_REASON_PLAY then
			local slash = sgs.Sanguosha:cloneCard("slash", originalCard:getSuit(), originalCard:getNumber())
			slash:addSubcard(originalCard:getId())
			slash:setSkillName(self:objectName())
			return slash
		elseif (reason == sgs.CardUseStruct_CARD_USE_REASON_RESPONSE) or (reason == sgs.CardUseStruct_CARD_USE_REASON_RESPONSE_USE) then
			local rescard = sgs.Sanguosha:cloneCard(pattern, originalCard:getSuit(), originalCard:getNumber())
			rescard:addSubcard(originalCard:getId())
			rescard:setSkillName(self:objectName())
			return rescard
		end
	end,
	enabled_at_play = function(self, player)
		return sgs.Slash_IsAvailable(player)
	end, 
	enabled_at_response = function(self, player, pattern)
		return (pattern == "slash") or (pattern == "jink")
	end,
}
ak_CangshanTMS = sgs.CreateTargetModSkill{
	name = "#ak_CangshanTMS",
	distance_limit_func = function(self, from, card)
		if from:hasSkill("ak_Cangshan") then
			if card:getSkillName() == "ak_Cangshan" then
				return 1000
			end
		end
	end
}
ak_CangshanTrig = sgs.CreateTriggerSkill{
	name="#ak_CangshanTrig",
	Frequency=sgs.Skill_Compulsory,
	events={sgs.CardResponded,sgs.CardUsed},
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		local card = nil
		if event == sgs.CardResponded then
			card = data:toCardResponse().m_card
		elseif event == sgs.CardUsed then
			card = data:toCardUse().card
		end
		if card:getSkillName() == "ak_Cangshan" then
			local x = 1
			if player:getPhase() == sgs.Player_NotActive then
				 x = 2
			end
			player:drawCards(x)
		end
	end,
}
--------------------------------------------------------------月煌@kntsubasa
ak_Yuehuang = sgs.CreateViewAsSkill{
	name = "ak_Yuehuang",
	n = 0,
	view_as = function(self, cards) 
		return ak_YuehuangCard:clone()
	end, 
	enabled_at_play = function(self, player)
		return not player:hasFlag("SucYh")
	end
}
ak_YuehuangCard = sgs.CreateSkillCard{
	name = "ak_YuehuangCard",
	target_fixed = true, 
	will_throw = true, 	
	on_use = function(self, room, source, targets)
		room:broadcastSkillInvoke("ak_Yuehuang",math.random(1,2))
		ResPlayers,targets = getmoesenlist(room,source,"@zessho")
		if ResPlayers:length() > 3 then return end
		room:setPlayerFlag(source,"SucYh")
		ResPlayers:removeOne(source)
		for _, p in sgs.qlist(ResPlayers) do
			local prompt = string.format("@YuehuangGive:%s:%s",p:objectName(),source:objectName())
			local card = room:askForCard(p,"EquipCard|.|.",prompt,sgs.QVariant(), sgs.Card_MethodResponse)
			if card then
				room:obtainCard(source,card)
				room:setPlayerMark(source,"yuehuang",source:getMark("yuehuang")+1)
			end
		end
		ResPlayers:append(source)
		for _,p in sgs.qlist(ResPlayers) do
			p:drawCards(source:getMark("yuehuang")+1)
		end
	end
}
ak_YuehuangSlash = sgs.CreateTargetModSkill{
	name = "#ak_YuehuangSlash",
	pattern = "Slash",
	residue_func = function(self, player)
		if player:hasSkill(self:objectName()) then
			if player:hasFlag("SucYh") then
				return player:getMark("yuehuang")+1
			end
		end
	end,
}
ak_YuehuangClear = sgs.CreateTriggerSkill{
	name= "#ak_YuehuangClear",
	frequency = sgs.Skill_Compulsory,
	events= {sgs.EventPhaseEnd},
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		if event == sgs.EventPhaseEnd then
			local phase = player:getPhase()
			if phase == sgs.Player_Finish then
				if player:hasSkill(self:objectName()) then
					room:setPlayerMark(player,"yuehuang",0)
				end
			end
		end
	end,
}
--------------------------------------------------------------镜鸣@khntmiku
ak_Jingming=sgs.CreateTriggerSkill{
	name = "ak_Jingming",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.EventPhaseChanging},
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		local change = data:toPhaseChange()
		local miku = room:findPlayerBySkillName(self:objectName())
		local newdata = sgs.QVariant()
		newdata:setValue(player)
		if change.to == sgs.Player_Start then
			if player:objectName() == miku:objectName() then return end
			if miku:getCardCount(true) == 0 then return end
			local prompt = string.format("@jmdiscard:%s:%s", miku:objectName(), player:objectName())
			if not room:askForCard(miku,"..",prompt,newdata,self:objectName()) then return end
			room:broadcastSkillInvoke("ak_Jingming",1)
			room:setPlayerMark(player,"noslash_jm",1)
			room:setPlayerCardLimitation(player, "use", "Slash", true)
		elseif change.from == sgs.Player_Finish then
			if player:getMark("noslash_jm") == 0 then return end
			if player:objectName() == miku:objectName() then return end
			room:setPlayerMark(player,"noslash_jm",0)
			local choice
			room:broadcastSkillInvoke("ak_Jingming",2)
			if player:isWounded() then
				choice = room:askForChoice(miku, self:objectName(), "recover+eachdraw", newdata)
			else 
				choice = "eachdraw"
			end
			if choice == "recover" then
				local rcv = sgs.RecoverStruct()
				rcv.recover = 1
				rcv.who = player
				room:recover(miku,rcv)
			elseif choice == "eachdraw" then
				player:drawCards(2)
			end
		end
	end,
	can_trigger = function(self, target)
		if target then
			return not target:hasSkill(self:objectName())
		end
		return false
	end,
}
--------------------------------------------------------------映现@khntmiku
ak_Yingxian = sgs.CreateViewAsSkill{
	name = "ak_Yingxian",
	n = 0,
	view_as = function(self, cards) 
		return ak_YingxianCard:clone()
	end, 
	enabled_at_play = function(self, player)
		return not player:hasFlag("Sucyx")
	end
}
ak_YingxianCard = sgs.CreateSkillCard{
	name = "ak_YingxianCard",
	target_fixed = true, 
	will_throw = true, 	
	on_use = function(self, room, source, targets)
		room:broadcastSkillInvoke("ak_Yingxian",math.random(1,2))
		ResPlayers,targets = getmoesenlist(room,source,"@zessho")
		if ResPlayers:length() > 3 or (ResPlayers:length() == 0) then return end
		room:setPlayerFlag(source,"Sucyx")
		local yxnum = 0
		for _, p in sgs.qlist(ResPlayers) do
			if p:getHandcardNum() > 0 then
				local card = room:askForCardShow(source,p,self:objectName())
				room:showCard(p,card:getEffectiveId())
				local point = card:getNumber()
				yxnum = yxnum + point
			end
		end
		local x = math.ceil(math.pow(yxnum,0.5))
		local cards = room:getNCards(x)
		local move = sgs.CardsMoveStruct()
		move.card_ids = cards
		move.to = source
		move.to_place = sgs.Player_PlaceHand
		move.reason = sgs.CardMoveReason(sgs.CardMoveReason_S_REASON_SHOW, source:objectName(), self:objectName(), nil)
		room:moveCards(move, false)
		while room:askForYiji(source,cards, self:objectName(), false, false, false, -1, ResPlayers) do end
	end
}
--------------------------------------------------------------扫射@yukinechris
ak_SaosheCard = sgs.CreateSkillCard{
	name = "ak_SaosheCard", 
	target_fixed = false, 
	will_throw = false, 
	filter = function(self, targets, to_select) 
		if #targets < 1 then
			if to_select:objectName() ~= sgs.Self:objectName() then
				local noneslash = sgs.Sanguosha:cloneCard("Slash")
				if to_select:isCardLimited(noneslash, sgs.Card_MethodUse) then return false end
				if to_select:isProhibited(sgs.Self, noneslash, sgs.Self:getSiblings()) then return false end
				if to_select:getMark("SaosheX") > 2 then return false end
				return sgs.Self:distanceTo(to_select) <= sgs.Self:getAttackRange()
			end
		end
		return false
	end,
	on_use = function(self, room, source, targets)
		local target = targets[1]
		room:setPlayerMark(target,"SaosheX",target:getMark("SaosheX")+1)
		room:obtainCard(target, self, false)
		local subcards = self:getSubcards()
		local old_value = source:getMark("Saoshe")
		local new_value = old_value + subcards:length()
		room:setPlayerMark(source, "Saoshe", new_value)
		if old_value < 2 then
			if new_value >= 2 then
				room:broadcastSkillInvoke("ak_Saoshe",1)
				if source:isWounded() then
					if room:askForChoice(source, self:objectName(), "recover+draw") == "recover" then
						local recover = sgs.RecoverStruct()
						recover.who = source
						room:recover(source, recover)
					else
						room:drawCards(source, 2)
					end
				else
					room:drawCards(source,2)
				end
			end
		end
		local slash = sgs.Sanguosha:cloneCard("slash", sgs.Card_NoSuit, 0)
		slash:setSkillName("ak_Saoshe")
		room:useCard(sgs.CardUseStruct(slash, source, target))
		room:addPlayerHistory(source, slash:getClassName(),-1)
	end
}
ak_SaosheVS = sgs.CreateViewAsSkill{
	name = "ak_Saoshe", 
	n = 1, 
	view_filter = function(self, selected, to_select)
		return true
	end,
	view_as = function(self, cards) 
		if #cards > 0 then
			local Saoshe_card = ak_SaosheCard:clone()
			local id = cards[1]:getId()
			Saoshe_card:addSubcard(id)
			return Saoshe_card
		end
	end
}
ak_Saoshe = sgs.CreateTriggerSkill{
	name = "ak_Saoshe", 
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.EventPhaseStart},  
	view_as_skill = ak_SaosheVS, 
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		room:setPlayerMark(player, "Saoshe", 0)
		room:setPlayerMark(player, "SaosheX",0)
		return false
	end, 
	can_trigger = function(self, target)
		if target then
			if target:isAlive() then
				if target:getPhase() == sgs.Player_NotActive then
					return true
				end
			end
		end
		return false
	end
}
--------------------------------------------------------------敌忾@yukinechris
ak_Dikai=sgs.CreateTriggerSkill{
	name = "ak_Dikai",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.Damaged},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local damage = data:toDamage()
		local yukine = room:findPlayerBySkillName(self:objectName())
		if not room:askForSkillInvoke(player, self:objectName(), data) then return end
		room:broadcastSkillInvoke("ak_Dikai",math.random(1,2))
		room:drawCards(yukine,1)
		local card_id = room:askForCardChosen(yukine,damage.from,"he",self:objectName())
		room:throwCard(card_id,damage.from,yukine)
		if yukine:objectName() ~= player:objectName() then
			local prompt = string.format("@dikai:%s:%s",yukine:getGeneralName(),player:objectName())
			local card = room:askForCard(yukine,"EquipCard,Slash|.|.",prompt,data,self:objectName())
			player:obtainCard(card,true)
		end
	end,
	can_trigger = function(self, target)
		if target then
			if target:isAlive() then
				if target:getMark("@zessho") > 0 then
					return target
				end
			end
		end
		return false
	end,
}
--------------------------------------------------------------缨枪@kyouko
ak_Yingqiang = sgs.CreateViewAsSkill{
	name = "ak_Yingqiang",
	n = 4,
	view_filter = function(self, selected, to_select)
		if #selected >= 4 then return false end
		if to_select:isEquipped() then return false end
		for _,card in ipairs(selected) do
			if card:getSuit() == to_select:getSuit() then return false end
		end
		return true
	end,
	view_as = function(self, cards)
		if #cards > 0 then
			local cardb
			local cardc
			local cardd
			local carda = cards[1]
			if #cards > 1 then
				cardb = cards[2]
				if #cards > 2 then
					cardc = cards[3]
					if #cards > 3 then
						cardd = cards[4]
					end
				end
			end
			local suit = sgs.Card_NoSuit
			local number = 0
			if #cards == 1 then
				suit = carda:getSuit()
				number = carda:getNumber()
			end
			local slash = sgs.Sanguosha:cloneCard("slash", sgs.Card_NoSuit, number)
			slash:setSkillName(self:objectName())
			for _,card in ipairs(cards) do
				slash:addSubcard(card)
			end
			return slash
		end
	end,
	enabled_at_play = function(self, player)
		return not player:hasFlag("YingqiangS")
	end,
}
ak_YingqiangTrig = sgs.CreateTriggerSkill{
	name = "#ak_YingqiangTrig",
	frequency = sgs.Skill_Compulsory,
	events = {sgs.TargetConfirmed,sgs.CardFinished,sgs.SlashProceed,sgs.Damage},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.TargetConfirmed then
			local use = data:toCardUse()
			local card = use.card
			if card:isKindOf("Slash") then
				if use.from:objectName() ~= player:objectName() then return end
				if card:isKindOf("Slash") then
					if card:getSkillName() == "ak_Yingqiang" then
						room:setPlayerFlag(player,"YingqiangS")
						for _, id in sgs.qlist(card:getSubcards()) do
							room:setPlayerMark(player,sgs.Sanguosha:getCard(id):getSuitString(),1)
						end
						if player:getMark("diamond") > 0 then
							room:setCardFlag(card, "WushuangInvke")
						end
						if player:getMark("club") > 0 then
							for _,p in sgs.qlist(use.to) do
								room:setPlayerMark(p, "Armor_Nullified", 1) 
							end
						end
					end
				end
			end
		elseif event == sgs.CardFinished then
			local use = data:toCardUse()
			local card = use.card
			if card:isKindOf("Slash") and (card:getSkillName() == "ak_Yingqiang") and (player:getMark("club") > 0) then
				for _,p in sgs.qlist(use.to) do
					room:setPlayerMark(p, "Armor_Nullified", 0)
				end
			end
			if card:getSkillName() == "ak_Yingqiang" then
				for _, id in sgs.qlist(card:getSubcards()) do
					room:setPlayerMark(player,sgs.Sanguosha:getCard(id):getSuitString(),0)
				end
			end
		elseif event == sgs.SlashProceed then
			if player:getMark("diamond") > 0 then
				local effect = data:toSlashEffect()
				local dest = effect.to
				if effect.slash:hasFlag("WushuangInvke") then
					local slasher = player:objectName()
					local hint = string.format("@wushuang-jink-1:%s", slasher)
					local first_jink = room:askForCard(dest, "jink", hint, sgs.QVariant(), sgs.CardUsed, player)
					local second_jink = nil
					if first_jink then
						hint = string.format("@wushuang-jink-2:%s", slasher)
						second_jink = room:askForCard(dest, "jink", hint, sgs.QVariant(), sgs.CardUsed, player)
					end
					local jink = nil
					if first_jink and second_jink then
						jink = sgs.Sanguosha:cloneCard("Jink", sgs.Card_NoSuit, 0)
						jink:addSubcard(first_jink)
						jink:addSubcard(second_jink)
					end
					room:slashResult(effect, jink)
				end
				return true
			end
		elseif event == sgs.Damage then
			local damage = data:toDamage()
			if damage.card and damage.card:isKindOf("Slash") and (damage.from:getMark("spade")>0) then
				local recover = sgs.RecoverStruct()
				recover.who = player
				room:recover(player, recover)
			end
			if damage.card and damage.card:isKindOf("Slash") and (damage.from:getMark("heart")>0) and (not damage.chain) and (not damage.transfer) and damage.to:isAlive() then
				local x = math.min(5, damage.to:getHp())
				damage.to:drawCards(x)
				damage.to:turnOver()
			end
		end
	end,
}
ak_YingqiangTMS = sgs.CreateTargetModSkill{
	name = "#ak_YingqiangTMS",
	pattern = "Slash",
	distance_limit_func = function(self, from, card)
		if from:hasSkill("ak_Yingqiang") and (card:getSkillName() == "ak_Yingqiang")  then
			return 1000
		else
			return 0
		end
	end,
}
--------------------------------------------------------------食餍@kyouko
ak_Shiyan = sgs.CreateTriggerSkill{
	name = "ak_Shiyan" ,
	events = {sgs.CardsMoveOneTime,sgs.EventPhaseStart,sgs.Damaged},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local kyouko = room:findPlayerBySkillName(self:objectName())
		if not kyouko then return end
		if event == sgs.CardsMoveOneTime then
			if player:objectName() ~= kyouko:objectName() then return end
			local move = data:toMoveOneTime()
			if move.from:objectName() ~= kyouko:objectName() then return end
			if (move.from_places:contains(sgs.Player_PlaceHand) or move.from_places:contains(sgs.Player_PlaceEquip)) and (move.to_place ~= sgs.Player_PlaceHand or move.to_place ~= sgs.Player_PlaceEquip)  then
				if not room:askForSkillInvoke(kyouko, self:objectName(), data) then return end
				local ids = room:getNCards(1)
				if ids:length() == 0 then return end
				room:broadcastSkillInvoke("ak_Shiyan",1)
				kyouko:addToPile("magika", ids)
			end
		elseif event == sgs.EventPhaseStart then
			if player:objectName() ~= kyouko:objectName() then return end
			if kyouko:getPhase() ~= sgs.Player_Start then return end
			if kyouko:getPile("magika"):length() > 0 then
				local x = 0
				for _,id in sgs.qlist(kyouko:getPile("magika")) do
					room:fillAG(kyouko:getPile("magika"), kyouko)
					local card_id = room:askForAG(kyouko, kyouko:getPile("magika"), false, self:objectName())
					room:clearAG(kyouko)
					room:obtainCard(kyouko,card_id,true)
					x = x + 1
					if x > 2 or kyouko:getPile("magika"):length() == 0 then
						break
					end
				end
				for _,id in sgs.qlist(kyouko:getPile("magika")) do
					room:throwCard(id,kyouko,kyouko)
				end
			end
		elseif event == sgs.Damaged then
			if (kyouko:getPile("magika"):length() > 0) then
				local damage = data:toDamage()
				local magika = kyouko:getPile("magika")
				if damage.to:objectName() ~= player:objectName() then return end
				if not room:askForSkillInvoke(kyouko, self:objectName(), data) then return end
				room:broadcastSkillInvoke("ak_Shiyan",2)
				room:fillAG(magika, kyouko)
				local card_id = room:askForAG(kyouko, magika, false, self:objectName())
				room:clearAG(kyouko)
				room:obtainCard(damage.to,card_id,true)
				if sgs.Sanguosha:getCard(card_id):getSuit() == sgs.Card_Heart then
					local recover = sgs.RecoverStruct()
					recover.who = kyouko
					room:recover(player, recover)
				end
			end
		end
	end,
	can_trigger = function(self, target)
		if target then
			if target:isAlive() then
				if target:getMark("@mahoushoujo") > 0 then
					return target
				end
			end
		end
		return false
	end,
}
--------------------------------------------------------------魔法少女@魔法少女
ak_Mahou = sgs.CreateTriggerSkill{
	name = "#ak_Mahou",
	events = {sgs.GameStart},
	frequency = sgs.Skill_Compulsory,
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		room:addPlayerMark(player,"@mahoushoujo",1)
	end,
	priority = 3
}
--------------------------------------------------------------明策@sagara
ak_MingceCard = sgs.CreateSkillCard{
	name = "ak_MingceCard" ,
	will_throw = false ,
	handling_method = sgs.Card_MethodNone ,
	on_effect = function(self, effect)
		local room = effect.to:getRoom()
		local targets = sgs.SPlayerList()
		if sgs.Slash_IsAvailable(effect.to) then
			for _, p in sgs.qlist(room:getOtherPlayers(effect.to)) do
				if effect.to:canSlash(p) then
					targets:append(p)
				end
			end
		end
		local target
		local choicelist = {"draw"}
		if (not targets:isEmpty()) and effect.from:isAlive() then
			target = room:askForPlayerChosen(effect.from, targets, self:objectName(), "@dummy-slash2:" .. effect.to:objectName())
			target:setFlags("ak_MingceTarget")
			table.insert(choicelist, "use")
		end
		effect.to:obtainCard(self)
		local choice = room:askForChoice(effect.to, self:objectName(), table.concat(choicelist, "+"))
		if target and target:hasFlag("ak_MingceTarget") then target:setFlags("-ak_MingceTarget") end
		if choice == "use" then
			if effect.to:canSlash(target, nil, false) then
				local slash = sgs.Sanguosha:cloneCard("thunder_slash", sgs.Card_NoSuit, 0)
				slash:setSkillName("_ak_Mingce")
				room:useCard(sgs.CardUseStruct(slash, effect.to, target), false)
			end
		elseif choice == "draw" then
			effect.to:drawCards(1)
		end
	end
}
ak_Mingce = sgs.CreateViewAsSkill{
	name = "ak_Mingce" ,
	n = 1 ,
	view_filter = function(self, selected, to_select)
		return to_select:isKindOf("EquipCard") or to_select:isKindOf("Slash")
	end ,
	view_as = function(self, cards)
		if #cards ~= 1 then return nil end
		local mingcecard = ak_MingceCard:clone()
		mingcecard:addSubcard(cards[1])
		return mingcecard
	end ,
	enabled_at_play = function(self, player)
		return not player:hasUsed("#ak_MingceCard")
	end
}
--------------------------------------------------------------筑楼@sagara
ak_XZhulou = sgs.CreateTriggerSkill{
	name = "ak_XZhulou",  
	frequency = sgs.Skill_NotFrequent, 
	events = {sgs.EventPhaseStart},  
	on_trigger = function(self, event, player, data) 
		local room = player:getRoom();
		if player:getPhase() == sgs.Player_Finish then
			if player:askForSkillInvoke(self:objectName()) then
				player:drawCards(2)
				if not room:askForCard(player, ".Weapon", "@zhulou-discard", sgs.QVariant(), sgs.CardDiscarded) then
					room:loseHp(player)
				end
			end
		end
		return false
	end
}
--------------------------------------------------------------刀狩@sagara
ak_DaoshouVS = sgs.CreateViewAsSkill{
	name = "ak_Daoshou", 
	n = 0, 
	view_filter = function(self, selected, to_select)
		return false
	end, 
	view_as = function(self, cards) 
		if #cards == 0 then
			local chain = sgs.Sanguosha:cloneCard("iron_chain", sgs.Card_NoSuit, 0)
			chain:setSkillName(self:objectName())
			return chain
		end
	end,
	enabled_at_response = function(self, player, pattern)
		return pattern == "@@ak_Daoshou"
	end,
	enabled_at_play = function(self, player)
		return false
	end, 
}
ak_Daoshou = sgs.CreateTriggerSkill{
	name = "ak_Daoshou",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.HpChanged},
	view_as_skill = ak_DaoshouVS, 
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		if not player:askForSkillInvoke(self:objectName()) then return end
		local choice
		local targets = sgs.SPlayerList()
		for _,p in sgs.qlist(room:getOtherPlayers(player)) do
			if p:getWeapon() ~= nil then
				targets:append(p)
			end
		end
		if targets:isEmpty() then
			choice = "tochain"
		else
			choice = room:askForChoice(player, self:objectName(), "togetweapon+tochain", data)
		end
		if choice == "tochain" then
			room:askForUseCard(player, "@@ak_Daoshou", "@tochain")
		elseif choice == "togetweapon" then
			local target = room:askForPlayerChosen(player, targets, self:objectName())
			player:obtainCard(target:getWeapon())
		end
	end,
}
--------------------------------------------------------------冰山@toumakazusa
ak_Iceberg = sgs.CreateTriggerSkill{
	name = "ak_Iceberg",
	frequency = sgs.Skill_Compulsory,
	events = {sgs.TargetConfirmed},
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		local kazusa = room:findPlayerBySkillName(self:objectName())
		local use = data:toCardUse()
		local source = use.from
		local targets = use.to
		if not use.card:isBlack() then return end
		if source:objectName() == kazusa:objectName() or targets:contains(kazusa) then
			for _,target in sgs.qlist(targets) do
				if target:objectName() == kazusa:objectName() then
					if not target:isKongcheng() then
						local hcid = room:askForExchange(kazusa, self:objectName(), 1, false,"kazusa_iceput",false):getSubcards():first()
						kazusa:addToPile("ak_ice", hcid)
					end
				elseif not target:isKongcheng() then
					local hcid = target:getRandomHandCardId()
					kazusa:addToPile("ak_ice", hcid)
				end
			end
		end
	end,
}
ak_IcebergDis = sgs.CreateDistanceSkill{
	name = "#ak_IcebergDis",
	correct_func = function(self, from, to)
		if to:hasSkill("ak_Iceberg") then
			if to:isKongcheng() then
				return to:getPile("ak_ice"):length()
			end
		end
	end,
}
--------------------------------------------------------------纯粹@toumakazusa
ak_PureVS = sgs.CreateViewAsSkill{
	name = "ak_PureVS", 
	n = 0,
	view_as = function(self, cards)
		if #cards == 0 then
			local pure
			if sgs.Sanguosha:getCurrentCardUsePattern() == "@@ak_pureslash" then
				pure = sgs.Sanguosha:cloneCard("Slash", sgs.Card_NoSuit, 0)
				pure:setSkillName("ak_Pure")
				return pure
			elseif sgs.Sanguosha:getCurrentCardUsePattern() == "@@ak_puretrick" then
				local purepattern = sgs.Self:property("puretrick"):toString()
				if purepattern ~= nil then
					pure = sgs.Sanguosha:cloneCard(purepattern,sgs.Card_NoSuit , 0)
					pure:setSkillName("ak_Pure")
					return pure
				end
			end
		end
	end,
	enabled_at_response = function(self, player, pattern)
		return pattern == "@@ak_pureslash" or pattern == "@@ak_puretrick"
	end,
	enabled_at_play = function(self, player)
		return false
	end, 
}
ak_PureAdd = sgs.CreateTriggerSkill{
	name = "#ak_PureAdd",
	events = {sgs.GameStart,sgs.EventLoseSkill},
	on_trigger = function(self,event,player,data)
		local room = player:getRoom()
		for _,p in sgs.qlist(room:getAlivePlayers()) do
			if not p:hasSkill("ak_PureVS") then
				room:acquireSkill(p,ak_PureVS)
			end
		end
	end,
}
ak_PureCard = sgs.CreateSkillCard{
	name = "ak_PureCard", 
	target_fixed = false, 
	will_throw = false, 
	filter = function(self, targets, to_select)
		if #targets == 0 then
			return to_select:objectName() ~= sgs.Self:objectName()
		end
		return false
	end,
	on_use = function(self, room, source, targets)
		local target = targets[1]
		local ice = source:getPile("ak_ice")
		local hcnum = source:getHandcardNum()
		if hcnum > ice:length() then
			room:obtainCard(target,source:wholeHandCards(),false)
			if hcnum > target:getHp() then
				local slash = sgs.Sanguosha:cloneCard("slash", sgs.Card_NoSuit, 0)
				local slashtars = sgs.PlayerList()
				for _,p in sgs.qlist(room:getAlivePlayers()) do
					if slash:targetFilter(slashtars, p, target) then
						slashtars:append(p)
					end
				end
				if slashtars:length() == 0 then return end
				purepattern = "slash"
				room:askForUseCard(target, "@@ak_pureslash", "@pureslash")
			end
		elseif hcnum < ice:length() then
			local icemove = sgs.CardsMoveStruct()
			for _,id in sgs.qlist(ice) do
				icemove.card_ids:append(id)
			end
			icemove.to = target
			icemove.from = source
			icemove.to_place = sgs.Player_PlaceHand
			room:moveCards(icemove,true,false)
			if ice:length() > target:getHp() then
				local patterns = {"dismantlement", "snatch", "ex_nihilo", "collateral", "duel", "fire_attack", "archery_attack", "savage_assault", "amazing_grace", "god_salvation", "iron_chain"}
				local choices = {}
				for _,p in ipairs(patterns) do
					if sgs.Sanguosha:cloneCard(p):isAvailable(target) then
						table.insert(choices, p)
					end
				end
				room:setPlayerProperty(target,"puretrick",sgs.QVariant())
				if #choices == 0 then return end
				local choice = room:askForChoice(target,"ak_Pure",table.concat(choices,"+"))
				room:setPlayerProperty(target,"puretrick",sgs.QVariant(choice))
				room:askForUseCard(target,"@@ak_puretrick","@puretrick")
			end
		end
	end,
}
ak_Pure = sgs.CreateViewAsSkill{
	name = "ak_Pure", 
	n = 0, 
	view_as = function(self, cards)
		return ak_PureCard:clone()
	end, 
	enabled_at_play = function(self, player)
		if not player:hasUsed("#ak_PureCard") then 
			return player:getHandcardNum() ~= player:getPile("ak_ice"):length()
		end
		return false
	end
}
--------------------------------------------------------------毅韧@naoeriki
ak_Yiren = sgs.CreateTriggerSkill{
	name = "ak_Yiren",
	frequency = sgs.Skill_Compulsory,
	events = {sgs.EventPhaseStart},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		local phase = player:getPhase()
		if phase == sgs.Player_Start then
			local list = room:getOtherPlayers(player)
			local cantrigger = true
			for _,p in sgs.qlist(list) do
				if p:getLostHp() > player:getLostHp() then
					cantrigger = false
					break
				end
			end
			for _,p in sgs.qlist(list) do
				if p:getMaxHp() > player:getMaxHp() then
					cantrigger = true
					break
				end
			end
			if cantrigger then
				local result
				if player:isWounded() then 
					result = room:askForChoice(player, self:objectName(), "chp+cmaxhp")
				else
					result = "cmaxhp"
				end
				if result == "chp" then
					local recover = sgs.RecoverStruct()
					recover.who = player
					recover.recover = 1
					room:recover(player, recover)
				else
					room:setPlayerProperty(player, "maxhp", sgs.QVariant(player:getMaxHp() + 2))
				end
			end
			return false
		end
	end
}
--------------------------------------------------------------炼心@naoeriki
ak_Lianxin = sgs.CreateTriggerSkill{
	name = "ak_Lianxin",
	frequency = sgs.Skill_NotFrequent,
	events = {sgs.EventPhaseChanging,sgs.CardUsed,sgs.CardResponded},
	on_trigger = function(self, event, player, data)
		local room = player:getRoom()
		if event == sgs.CardUsed or event == sgs.CardResponded then
			if player:getPhase() ~= sgs.Player_NotActive then
				local card
				if event == sgs.CardUsed then
					card = data:toCardUse().card
				elseif event == sgs.CardResponded then
					card = data:toCardResponse().m_card
				end
				local no = card:getNumber()
				room:setPlayerMark(player,"lianxinusecard",player:getMark("lianxinusecard") + 1 )
				if no > 0 and (not player:hasFlag("lianxinfailed")) then
					if no > player:getMark("lianxinno") then
						room:setPlayerMark(player,"lianxinno",no)
					elseif player:getMark("lianxinno") >= no then
						room:setPlayerFlag(player,"lianxinfailed")
						room:setPlayerMark(player,"lianxinno",0)
					end
				end
			end
		elseif event == sgs.EventPhaseChanging then
			local change = data:toPhaseChange()
			if change.to == sgs.Player_NotActive then
				if player:getMark("lianxinusecard") > 1 then
					room:setPlayerMark(player,"lianxinusecard",0)
					if player:getMark("lianxinno") > 0 and (not player:hasFlag("lianxinfailed")) then
						room:setPlayerMark(player,"lianxinno",0)
						if not room:askForSkillInvoke(player,self:objectName()) then return end
						room:loseHp(player,1)
						room:setPlayerProperty(player, "maxhp", sgs.QVariant(player:getMaxHp() - 1))
						player:gainAnExtraTurn()
					end
				end
			end
		end
	end		
}
------------------------------------------------------------------------翻译添加区
yj:addSkill(GuanchuanSkill)
yj:addSkill(ZhuishaSkill)
yj:addSkill(CixiongSkill)
yj:addSkill(HuoshanSkill)
yj:addSkill(BaojiSkill)
yj:addSkill(HanbingSkill)
yj:addSkill(ShemaSkill)
yj:addSkill(ZhangbaSkill)
yj:addSkill(HuajiSkill)
yj:addSkill(ak_Chitian)
yj:addSkill(ak_Jianyu)
yj:addSkill(ak_JiguangAsk)
yj:addSkill(ak_Gaokang)
yj:addSkill(ak_Gaoxiao)
yj:addSkill(ak_Yinbao)
yj:addSkill(ak_Kuanggu)
yj:addSkill(ak_Wushuang)
yj:addSkill(ak_Paoxiao)
yj:addSkill(ak_Kuangshi)
yj:addSkill(ak_Xiuluo)
yj:addSkill(ak_PureVS)
itoumakoto:addSkill(ak_Renzha)
itoumakoto:addSkill(haochuantimes)
itoumakoto:addSkill(renzha)
extension:insertRelatedSkills("ak_Renzha","#haochuantimes")
ayanami:addSkill(weixiao)
ayanami:addSkill(nvshen)
keima:addSkill(ak_Shenzhi)
keima:addSkill(ak_Gonglve)
kiriko:addSkill(ak_ak47)
kiriko:addSkill(ak_Guangjian)
kiriko:addSkill(ak_GuangjianVS)
extension:insertRelatedSkills("ak_GuangjianVS","#ak_Guangjian")
odanobuna:addSkill(ak_Chigui)
odanobuna:addSkill(ak_Buwu)
odanobuna:addSkill(ak_Tianmo)
odanobuna:addSkill(ak_TianmoDefense)
extension:insertRelatedSkills("ak_TianmoDefense","#ak_Tianmo")
yuuta:addSkill(ak_Wangxiang)
yuuta:addSkill(ak_Blackflame)
tsukushi:addSkill(ak_Gqset)
tsukushi:addSkill(ak_Gqef)
extension:insertRelatedSkills("ak_Gqset","#ak_Gqef")
tsukushi:addSkill(ak_Tiaojiao)
batora:addSkill(ak_Buqi)
mao_maoyu:addSkill(ak_Boxue)
sheryl:addSkill(ak_Yaojing)
sheryl:addSkill(ak_Gongming)
aoitori:addSkill(ak_Luowang)
aoitori:addSkill(ak_FanxiangVS)
aoitori:addSkill(ak_Fanxiang)
extension:insertRelatedSkills("ak_FanxiangVS","#ak_Fanxiang")
kyouko:addSkill(ak_Yingqiang)
kyouko:addSkill(ak_YingqiangTrig)
kyouko:addSkill(ak_YingqiangTMS)
kyouko:addSkill(ak_Shiyan)
kyouko:addSkill(ak_Mahou)
extension:insertRelatedSkills("ak_Yingqiang","#ak_YingqiangTrig")
extension:insertRelatedSkills("ak_Yingqiang","#ak_YingqiangTMS")
diarmuid:addSkill(ak_Pomo)
diarmuid:addSkill(ak_Bimie)
diarmuid:addSkill(ak_BimieHprcvForbidden)
diarmuid:addSkill(ak_BimieLost)
extension:insertRelatedSkills("ak_Bimie","#ak_BimieHprcvForbidden")
extension:insertRelatedSkills("ak_Bimie","#ak_BimieLost")
ikarishinji:addSkill(ak_Baozou)
ikarishinji:addSkill(ak_Xinbi)
ikarishinji:addSkill(ak_BaozouDying)
ikarishinji:addSkill(ak_BaozouSound)
extension:insertRelatedSkills("ak_Baozou","#ak_BaozouDying")
extension:insertRelatedSkills("ak_Baozou","#ak_BaozouSound")
redarcher:addSkill(ak_TouyingVS)
redarcher:addSkill(ak_Gongqi)
redarcher:addSkill(ak_GongqiTargetMod)
redarcher:addSkill(ak_TouyingClear)
redarcher:addSkill(ak_Jianyong)
redarcher:addSkill(ak_Jianzhi)
extension:insertRelatedSkills("ak_Gongqi","#ak_Gongqi-target")
extension:insertRelatedSkills("ak_TouyingVS","#ak_TouyingClear")
redo:addSkill(ak_RedoWake)
redo:addSkill(ak_ChamberStart)
redo:addSkill(ak_ChamberMove)
redo:addSkill(ak_JiguangMod)
redo:addSkill(ak_RedoNoSlash)
extension:insertRelatedSkills("ak_ChamberMove","#ak_ChamberStart")
extension:insertRelatedSkills("ak_ChamberMove","#ak_RedoNoSlash")
extension:insertRelatedSkills("ak_JiguangAsk","#ak_JiguangMod")
runaria:addSkill(ak_YukongMod)
runaria:addSkill(ak_Yukong)
runaria:addSkill(ak_Wenle)
runaria:addSkill(ak_Qisi)
extension:insertRelatedSkills("ak_Yukong","#ak_YukongMod")
fuwaaika:addSkill(ak_Poshi)
fuwaaika:addSkill(ak_PoshiTMS)
fuwaaika:addSkill(ak_PoshiTrig)
fuwaaika:addSkill(ak_Liansuo)
fuwaaika:addSkill(ak_Yinguo)
extension:insertRelatedSkills("ak_Poshi","#ak_PoshiTMS")
extension:insertRelatedSkills("ak_Poshi","#ak_PoshiTrig")
slsty:addSkill(slyanhuo)
slsty:addSkill(zhahu)
rokushikimei:addSkill(ak_Heartlead)
rokushikimei:addSkill(ak_PositionMove)
rokushikimei:addSkill(ak_Spkprison)
bernkastel:addSkill(qiji)
bernkastel:addSkill(suipian)
hibiki:addSkill(ak_Synchrogazer)
hibiki:addSkill(ak_Zessho)
hibiki:addSkill(ak_Gungnir)
kntsubasa:addSkill(ak_Cangshan)
kntsubasa:addSkill(ak_CangshanTrig)
kntsubasa:addSkill(ak_CangshanTMS)
kntsubasa:addSkill(ak_Yuehuang)
kntsubasa:addSkill(ak_YuehuangSlash)
kntsubasa:addSkill(ak_YuehuangClear)
kntsubasa:addSkill(ak_Zessho)
extension:insertRelatedSkills("ak_Cangshan","#ak_CangshanTrig")
extension:insertRelatedSkills("ak_Cangshan","#ak_CangshanTMS")
extension:insertRelatedSkills("ak_Yuehuang","#ak_YuehuangSlash")
extension:insertRelatedSkills("ak_Yuehuang","#ak_YuehuangClear") 
khntmiku:addSkill(ak_Zessho)
khntmiku:addSkill(ak_Jingming)
khntmiku:addSkill(ak_Yingxian)
yukinechris:addSkill(ak_Zessho)
yukinechris:addSkill(ak_Saoshe)
yukinechris:addSkill(ak_Dikai)
sagara:addSkill(ak_Daoshou)
sagara:addSkill(ak_Mingce)
sagara:addSkill(ak_XZhulou)
toumakazusa:addSkill(ak_Pure)
toumakazusa:addSkill(ak_PureAdd)
toumakazusa:addSkill(ak_Iceberg)
toumakazusa:addSkill(ak_IcebergDis)
extension:insertRelatedSkills("ak_Pure","#ak_PureAdd")
extension:insertRelatedSkills("ak_Iceberg","#ak_IcebergDis")
naoeriki:addSkill(ak_Yiren)
naoeriki:addSkill(ak_Lianxin)
------------------------------------------------------------------------翻译表区
------------------------------------------------------------------------杂项区
sgs.LoadTranslationTable{
	["HuajiSkill"] = "方天画戟",
	["ShemaSkill"] = "麒麟弓",
	["HanbingSkill"] = "寒冰剑",
	["ZhangbaSkill"] = "丈八蛇矛",
	["CixiongSkill"] = "雌雄双股剑",
	["ZhuishaSkill"] = "青龙偃月刀",
	["GuanchuanSkill"] = "贯石斧",
	["BaojiSkill"] = "古锭刀",
	["HuoshanSkill"] = "朱雀羽扇",
	["@axe"] = "弃置两张牌以发动贯石斧效果",
	["ak_Kuanggu"]="狂骨",
	["ak_Xiuluo"]="修罗",
	["ak_Paoxiao"]="咆哮",
	["ak_Wushuang"]="无双",
	["@wushuang-jink-1"]="%src 发动技能【无双】要求你使用第一张闪",
	["@wushuang-jink-2"]="%src 发动技能【无双】要求你使用第二张闪",
	[":ak_Kuanggu"]="每当你对距离1以内的一名角色造成1点伤害后，你回复1点体力",
	[":ak_Xiuluo"]="准备阶段开始时，你可以弃置一张与判定区内延时类锦囊牌花色相同的手牌，然后弃置该延时类锦囊牌",
	[":ak_Paoxiao"]="你在出牌阶段内使用【杀】时无次数限制。",
	[":ak_Wushuang"]="当你使用【杀】指定一名角色为目标后，该角色需连续使用两张【闪】才能抵消；与你进行【决斗】的角色每次需连续打出两张【杀】。",
	["zha"] = "渣",
	["gang"]="钢",
	["yong"]="咏",
	["si"]="丝",
	["@tianmo"]="天魔",
	["@zhou"]="咒",
	["@Chamber"]="钱伯",
	["@miracle"]="奇迹",
	["@zessho"]="絶唱",
	["moesenskill"]="萌战技",
	["ak_TiaojiaoCard"]="调教 ",
	["ak_BlackflameCard"]="黑焰",
	["ak_GonglveCard"]="攻略",
	["ak_BoxueCard"]="博学",
	["ak_YuehuangCard"]="月煌",
	["ak_boxue"]="博学",
	["ak_jianyu"]="剑雨",
	["ak_renzha"]="好船",
	["ak_gonglve"]="攻略",
	["ak_gongqi"]="崩坏",
	["ak_blackflame"]="黑焰",
	["ak_tiaojiao"]="调教",
	["ak_touying"]="投影",
	["ak_yukong"]="御空",
	["ak_mingce"]="明策",
	["ak_synchrogazer"]="同步",
	["#ak_SynchrogazerCard"]="同步",
	["ak_SynchrogazerCard"]="同步",
	["ak_pure"]="纯粹",
	["ak_Synchrogazer_Target"]="请指定【杀】的目标。",
	["ak_Synchrogazer_Friend"]="请指定摸牌的目标。",
	["#ak_YuehuangCard"]="月煌",
	["#ak_ChamberStart"]="钱伯",
	["ak_yuehuang"]="月煌",
	["ak_yingxian"]="映现",
	["@LuoDraw"]="选择一名角色并令其摸一张牌",
	["@LuoDis"]="选择一名角色并弃置其一张牌",
	["@tochain"]="请选择【铁索连环】的目标角色。",
	["~ak_Daoshou"]="选择角色使其横置或重置之。",
	["@yinguo"]="请选择一名男性角色",
	["@yinguo2"]="请选择一名男性角色（若只分给一名角色，则选之前所选的角色。）",
	["@Buqigiving"]="【布棋】要求你( %src)交给%dest共%arg张手牌。",
	["@rokugive"]="【语言牢笼】要求你(%src)交给%dest共4张牌（包括装备牌）",
	["@TiaojiaoSlash"]=" %src 发动技能【调教】，要求你( %dest )对 %arg 使用1张杀。",
	["@LuoBaozouKill"]="选择一名角色令其进入濒死求桃",
	["@jiguang"]="%src 技能【激光】生效，你须弃置一张非基本牌才能抵消其杀。",
	["@SynSlash"] = "%src  发动技能【同步】让你对 %dest 使用一张杀。",
	["@YuehuangGive"] = "%dest发动技能【月煌】让你（%src）交给 %dest 一张装备牌 ",
	["@dikai"] = "你（%src） 发动技能【敌忾】，可交给 %dest 一张【杀】或一张装备牌。",
	["@jmdiscard"]="你（%src） 可弃置一张牌以发动技能【镜鸣】，使 %dest 不能使用【杀】直到回合结束。",
	["@zhulou-discard"]="请弃置一张武器牌，或失去1点体力。",
	["@pureslash"]="请选择【杀】的目标",
	["@puretrick"]="请视为使用一张非延时锦囊牌 技能来源:【纯粹】",
	["seeandchoose"]="观看当前回合角色所有手牌并选择一张获得",
	["drawone"]="摸一张牌",
	["a"]="令一名角色摸牌",
	["b"]="令一名角色弃牌",
	["recover"]="回复1点体力",
	["eachdraw"]="该角色摸两张牌",
	["draw"]="摸两张牌",
	["throw"]="弃置",
	["gx"]="以任意顺序置于牌堆顶",
	["youdraw"]="你摸X张牌",
	["hedraws"]="令其摸X张牌",
	["throwtableequip"]="弃置场上一张装备牌",
	["loseonehp"]="失去1点体力",
	["turnoverself"]="翻面",
	["chained"]="横置",
	["tochain"]="视为使用【铁索连环】",
	["togetweapon"]="获得一张武器牌",
	["chp"]="回复1点体力",
	["cmaxhp"]="加2体力上限",
	["#TianmoDefense"]="%from 失去1个’天魔‘标记，防止了此次扣减体力。 ",
	["kazusa_iceput"]="请选择一张手牌 技能来源：冰山",
	["#ak_Guangjian"]="光剑",
	["magika"]="环",
	["ak_ice"]="冰",
	["erciyuan"] = "二次元", 
	["Erciyuan"] = "二次元",
}
------------------------------------------------------------------------武将描述区
sgs.LoadTranslationTable{
	["keima"]="桂木桂马",
	["#keima"]="神大人",
	["ak_Gonglve"]="攻略",
	["ak_Shenzhi"]="神知",
	
	["#ayanami"] = "女神",
	["ayanami"]="绫波丽",
	["weixiao"]="微笑",
	["nvshen"]="女神",
	
	["#itoumakoto"] = "好船人渣",
	["itoumakoto"]="伊藤诚",
	["renzha"]="人渣",
	["ak_Renzha"]="好船",
	
	["kiriko"]="桐子",
	["#kiriko"]="光剑使",
	["ak_ak47"]="连斩",
	["ak_Guangjian"]="光剑",
	["ak_GuangjianVS"]="光剑",
	
	["odanobuna"]="织田信奈",
	["#odanobuna"]="第六天魔王",
	["ak_Buwu"]="布武",
	["ak_Chigui"]="赤鬼",
	["ak_TianmoDefense"]="天魔",
	
	["yuuta"]="富樫勇太",
	["#yuuta"]="漆黑烈焰使",
	["ak_Wangxiang"]="妄想",
	["ak_Blackflame"]="黑焰",
	
	["tsukushi"]="筒隐筑紫",
	["#tsukushi"]="钢铁之王",
	["ak_Gqset"]="钢躯",
	["ak_Tiaojiao"]="调教",
	
	["batora"]="魔·右代宫战人",
	["#batora"]="无限黄金魔术师",
	["ak_Buqi"]="布棋",
	
	["mao_maoyu"]="魔王",
	["#mao_maoyu"]="红玉之瞳",
	["ak_Boxue"]="博学",
	
	["sheryl"]="雪莉露·诺姆",
	["#sheryl"]="银河的妖精",
	["ak_Gongming"]="共鸣",
	["ak_Yaojing"]="妖精",
	
	["aoitori"]="葵·托利",
	["#aoitori"]="不可能之男",
	["ak_Luowang"]="裸王",
	["ak_FanxiangVS"]="泛象",
	
	["kyouko"]="佐仓杏子",
	["#kyouko"]="红色幽灵",
	["ak_Yingqiang"]="缨枪",
	["ak_Shiyan"]="食餍",
	
	["diarmuid"]="迪卢木多",
	["#diarmuid"]="光辉之貌",
	["ak_Pomo"]="破魔",
	["ak_Bimie"]="必灭",
	
	["ikarishinji"]="碇シンジ",
	["#ikarishinji"]="中二少年",
	["ak_Kuangshi"]="狂噬",
	["ak_Baozou"]="暴走",
	["ak_Xinbi"]="心壁",
	["ak_Yinbao"]="音爆",
	
	["redarcher"]="英灵卫宫",
	["#redarcher"]="冶炼的英雄",
	["ak_TouyingVS"]="投影",
	["ak_Gongqi"]="崩坏",
	["ak_Jianyong"]="剑咏",
	["ak_Jianyu"]="剑雨",
	["ak_Jianzhi"]="剑制",
	["ak_Chitian"]="炽天",
	
	["redo"]="雷德",
	["#redo"]="翠星的探索者",
	["ak_RedoWake"]="去死吧，铁皮混蛋！",
	["ak_ChamberMove"]="钱伯",
	["ak_Gaoxiao"]="高效",
	["ak_Gaokang"]="高抗",
	["ak_JiguangAsk"]="激光",
	
	["runaria"]="露娜莉亚",
	["#runaria"]="月长石",
	["ak_Wenle"]="文乐",
	["ak_Qisi"]="绮丝",
	["ak_Yukong"]="御空",
	
	["fuwaaika"]="不破爱花",
	["#fuwaaika"]="绝园的魔法使",
	["ak_Poshi"]="破始",
	["ak_Liansuo"]="连锁",
	["ak_Yinguo"]="因果",
	
	["slsty"]="塞蕾丝缇雅",
	["#slsty"]="超高校级的赌徒",
	["slyanhuo"]="言惑",
	["yanhuoAcquire"]="言惑翻牌",
	["zhahu"]="诈唬",
	
	["rokushikimei"]="六识命",
	["#rokushikimei"]="心灵梦魇",
	["ak_Heartlead"]="心灵诱导",
	["ak_PositionMove"]="藏位转移",
	["ak_Spkprison"]="语言牢笼",
	
	["bernkastel"]="贝伦卡斯泰露",
	["#bernkastel"]="奇迹之魔女",
	["qiji"]="奇迹宣言",
	["suipian"]="碎片筛选",
	
	["hibiki"]="立花響",
	["#hibiki"]="永恒之枪",
	["ak_Synchrogazer"]="同步",
	["ak_Gungnir"]="圣枪",
	
	["kntsubasa"]="風鳴翼",
	["#kntsubasa"]="天羽羽斩",
	["ak_Cangshan"]="苍闪",
	["ak_Yuehuang"]="月煌",
	
	["khntmiku"]="小日向未来",
	["#khntmiku"]="神兽镜",
	["ak_Jingming"]="镜鸣",
	["ak_Yingxian"]="映现",
	
	["yukinechris"]="雪音クリス",
	["#yukinechris"]="魔弓",
	["ak_Saoshe"]="扫射",
	["ak_Dikai"]="敌忾",
	
	["sagara"]="相良良晴",
	["#sagara"]="猴子太阁",
	["ak_XZhulou"]="筑楼",
	["ak_Mingce"]="明策",
	["ak_Daoshou"]="刀狩",
	
	["toumakazusa"]="冬馬和紗",
	["#toumakazusa"]="忠犬",
	["ak_Iceberg"]="冰山",
	["ak_Pure"]="纯粹",
	["ak_PureVS"]="纯粹用牌",
	
	["naoeriki"]="直枝理樹",
	["#naoeriki"]="普通的少年",
	["ak_Yiren"]="毅韧",
	["ak_Lianxin"]="炼心",
}
------------------------------------------------------------------------技能描述区
sgs.LoadTranslationTable{
	[":ak_Tiaojiao"]="<font color=\"green\"><b>出牌阶段限一次，</b></font>你可以令一名角色选择一项：对一名你指定的其他角色使用一张【杀】，或令你获得其区域内的一张牌。",
	[":ak_Wangxiang"]="当你的手牌数不多于你已损失的体力值时，你可以将一张手牌当【无中生有】使用。",
	[":ak_Gqset"]="结束阶段开始时，你可以将一张手牌置于你的武将牌上，称为“钢”，然后同类型的牌对你无效直至你下一回合的准备阶段。你的准备阶段开始时，若你的武将牌上有“钢”，须将“钢”弃置。（“钢”至多存在1张）",
	[":ak_Blackflame"]="<font color=\"green\"><b>出牌阶段限一次，</b></font>你可失去1点体力，然后对一名角色造成1点火焰伤害。",
	[":ak_Buwu"]="每当你造成一次伤害后，你可令受到伤害的角色将其武将牌翻面，然后该角色摸X张牌（X为该角色当前的体力值-1且至多为5）。",
	[":ak_Chigui"]="结束阶段开始时，你可失去1点体力，获得1名角色装备区内的武器牌，然后再摸1张牌，直到场上没有武器牌。",
	[":ak_TianmoDefense"]="当你使用的【杀】被【闪】抵消时，你获得1枚“天魔”标记。当扣减你的体力时，你可弃置1枚“天魔”标记，防止此次扣减体力。",	
	[":ak_GuangjianVS"]="出牌阶段开始时，你可以进行一次判定。若如此做，出牌阶段你可以将与结果花色相同的手牌当【杀】使用。",
	[":ak_ak47"]="每当你使用一张黑色【杀】指定唯一角色为目标后，你可以摸X张牌，然后此【杀】对目标角色额外结算X次（X为你已损失的体力值）。",
	[":ak_Renzha"]="<font color=\"green\"><b>出牌阶段限一次，</b></font>你可以弃置一张“渣”,然后回复X点体力并将你的武将牌翻面，视作你发动一次技能“乱武”。（X为“渣”的数量-1）",
	[":renzha"]="每当你受到1点伤害后，你摸两张牌并将一张手牌置于你的武将牌上，称为“渣”，然后你可将武将牌翻面并摸一张牌。",
	[":weixiao"]="结束阶段开始时，你可选择弃置两张牌然后选择一项；令一名角色摸X张牌，或令一名角色弃置Y张牌（X为弃牌中较小点数的一半，向下取整；Y为弃牌中较大点数的一半，向上取整）",
	[":ak_Shenzhi"]="其他角色的准备阶段开始时，你可以弃置一张牌，然后直到回合结束，每有一张与你弃置的牌类别相同的牌置入弃牌堆时，你选择一项：观看当前回合角色所有手牌，然后选择其中一张获得之，或摸一张牌。",
	[":ak_Gonglve"]="每当你对其他角色造成1点伤害后，或受到其他角色造成的1点伤害后，若该角色存活，你可以令你与其各摸一张牌。",
	[":nvshen"]="<font color=\"blue\"><b>锁定技，</b></font>你的手牌上限+X（X为你的体力上限）。",
	[":ak_Buqi"]="游戏开始时或你受到一次伤害后，你可获得所有其他角色手牌，并由当前回合角色开始，依次交给每名其他角色等同于其原有手牌张数的手牌。",
	[":ak_Boxue"]="<font color=\"green\"><b>出牌阶段限一次，</b></font>你可指定X名角色并亮出牌堆顶X张牌，然后你摸X/2(向上取整)张牌，指定角色依次与之交换一张牌（包括装备牌），然后你将其余的牌弃置，或以任意顺序置于牌堆顶。",
	[":ak_Yaojing"]="出牌阶段，你可将一张手牌当【桃园结义】使用。",
	[":ak_Gongming"]="你的回合内，每有一名角色回复体力，你可以令其摸X张牌，或你摸X张牌。（X为你已损失体力值）。",
	[":ak_Luowang"]="<font color=\"blue\"><b>锁定技，</b></font>当你没有牌时，你不能成为黑色牌和【杀】的目标。",
	[":ak_FanxiangVS"]="<font color=\"green\"><b>出牌阶段对每名角色限一次，</b></font>你可以将任意数量的牌交给一名角色，若交给其的牌的张数达到了两张或更多，你令其进行一个额外的回合。",
	[":ak_Pomo"]="<font color=\"blue\"><b>锁定技，</b></font>你使用【杀】时，无视目标角色的防具。",
	[":ak_Bimie"]="每当你使用的【杀】造成伤害后，你可令受到伤害的角色获得1枚“咒”标记，拥有该标记的角色回复体力时，取消之。你死亡时，弃置场上所有的”咒“标记。",
	[":ak_Baozou"]=" 你的濒死状态结算后，你可以暂停一切结算，发动以下效果且防止进入濒死状态直到该效果结束，然后继续暂停的结算：你进行一个额外的回合并于此回合内获得以下技能：“无双”、“咆哮”、“狂骨”、“猛噬”（锁定技，你的基本牌均视为【杀】，你的锦囊牌均视为【决斗】）“音爆”（锁定技，你计算与其他角色的距离始终为1）。",
	[":ak_Kuangshi"]="<font color=\"blue\"><b>锁定技，</b></font>你的基本牌均视为【杀】，你的锦囊牌均视为【决斗】。",
	[":ak_Xinbi"]="<font color=\"blue\"><b>锁定技，</b></font>你因“暴走”外的方式执行的回合的结束阶段开始时，你弃置场上一张装备牌或失去1点体力；<font color=\"blue\"><b>锁定技，</b></font>当你获得三张或更多牌时，每获得两张，你失去1点体力，当前回合角色摸一张牌。",
	[":ak_Yinbao"]="<font color=\"blue\"><b>锁定技，</b></font>你计算的与其他角色距离为1。",
	[":ak_TouyingVS"]="<font color=\"green\"><b>出牌阶段限一次，</b></font>若你的装备区没有武器，你可以声明一种武器，获得其特效直到回合结束。",
	[":ak_Gongqi"]="你可以将一张装备牌当无距离限制的【杀】使用。",
	[":ak_Jianyong"]="弃牌阶段开始时，若此回合内之前你未造成伤害，可以摸两张牌并将一张手牌置于武将牌上，称为”咏“。",
	[":ak_Jianyu"]="你可以将等同于存活角色数的”咏“置入弃牌堆，视为使用一张【万箭齐发】。",
	[":ak_Chitian"]="你可以将一张”咏“视作【杀】或【闪】打出。", 
	[":ak_Jianzhi"]="<font color=\"purple\"><b>觉醒技，</b></font>结束阶段开始时，若你的”咏“的数量达到三张或更多时，你减1点体力上限，获得每名其他角色各一张手牌并置于你的武将牌上，然后获得技能”剑雨“和”炽天“，此回合结束后进行一个额外的回合，",
	[":ak_RedoWake"]="<font color=\"purple\"><b>觉醒技，</b></font>出牌阶段开始时，若你的体力值为1，你失去“钱伯”标记，回复1点体力，然后对一名其他角色造成3点雷电伤害。",
	[":ak_ChamberMove"]="分发起始手牌时，少发你两张牌。游戏开始时，你获得标记“钱伯”，你可以于准备阶段开始时获得标记“钱伯”，或将其移出游戏：若你拥有“钱伯”标记，你拥有以下技能“激光”“高效”“高抗”；若你没有，你不能成为【杀】的目标。",
	[":ak_Gaoxiao"]="摸牌阶段摸牌时，你可以额外摸一张牌。",
	[":ak_JiguangAsk"]="<font color=\"blue\"><b>锁定技，</b></font>你使用【杀】时无距离限制，且目标角色须弃置一张非基本牌以抵消之",
	[":ak_Gaokang"]="<font color=\"blue\"><b>锁定技，</b></font>若你有手牌，你受到的非属性伤害-1且你弃置一张牌。",
	[":ak_Wenle"]="<font color=\"blue\"><b>锁定技，</b></font>摸牌阶段摸牌时，你额外摸X张牌，然后摸牌阶段结束时将X张牌背面朝上置于武将牌上称为“丝”（X为存活角色数的一半，向下取整）。准备阶段开始时，你须弃置所有的“丝”，然后摸等量的牌。",
	[":ak_Qisi"]="【杀】或非延时类锦囊对你生效前，你可以弃置一张“丝”，若该角色的攻击范围小于“丝”点数的一半（向下取整），取消之。",
	[":ak_Yukong"]="<font color=\"green\"><b>出牌阶段限一次，</b></font>你可以弃置一张“丝”，然后你计算和其他角色的距离为1，直到回合结束。",
	[":ak_Poshi"]="<font color=\"green\"><b>出牌阶段限一次，</b></font>你可失去1点体力，然后你无视与其他角色的距离和其他角色的防具；且你使用杀时无使用次数限制，直到回合结束。",
	[":ak_Liansuo"]="每名其他角色的回合限三次，其他角色的牌因弃置而置入弃牌堆时，你可以摸一张牌。",
	[":ak_Yinguo"]="<font color=\"red\"><b>限定技，</b></font>你死亡后，可以将所有手牌交给至多两名男性角色，然后指定的角色再摸等同于交给其手牌张数的牌。",
	[":slyanhuo"]="出牌阶段，你可以将一张手牌背面朝上置于武将牌上，称为“惑”（惑最多为当前场上人数且最多为4），然后你摸一张牌。其他角色的出牌阶段限一次，可以翻开一张你的“惑”。你受到伤害后，可令伤害来源翻开你指定的一张“惑”。",
	[":zhahu"]="<font color=\"blue\"><b>锁定技，</b></font>翻开“惑”的角色须执行以下效果并弃置“惑”，黑桃，回复1点体力，红桃，失去1点体力，梅花，摸两张牌，方块，你弃置其一张牌；准备阶段开始时，你须翻开你所有的“惑”。",
	[":ak_Heartlead"]="其他角色的基本牌或非延时类锦囊牌指定一名角色为目标时，你可以将武将牌翻面或横置，然后该角色指定另一名角色为此牌的唯一目标。",
	[":ak_PositionMove"]="<font color=\"blue\"><b>锁定技，</b></font>你受到的伤害结算开始时，若你有两张或更多手牌且你的武将牌横置或背面朝上，你弃置两张牌并防止此伤害，然后将你的武将牌翻至正面并重置。",
	[":ak_Spkprison"]="<font color=\"red\"><b>限定技，</b></font>一名其他角色的准备阶段开始时，你可以令其摸三张牌，然后该角色须交给你四张牌。",
	[":qiji"]="当一名角色受到其他角色伤害而进入濒死状态时，你可令该角色和伤害来源各进行一次判定；若判定点数相同，则该角色体力回复至其体力上限，伤害来源失去全部体力。（该技能对一名角色一局游戏只能使用一次）",
    [":suipian"]="在一名角色的判定牌生效前，你可用牌堆顶牌代替判定牌且你可重复此流程。",	
	[":ak_Gungnir"]="<font color=\"blue\"><b>锁定技，</b></font>你的【杀】不能被【闪】响应。你对体力值比你多的角色造成的伤害+1；你对体力值不多于你的角色造成伤害时，回复1点体力。回合内你每造成一次伤害，结束阶段开始时便须弃置一张牌。",
	[":ak_Synchrogazer"]="<font color=\"Sky Blue\"><b>萌战技,绝唱,3,出牌阶段限一次;</b></font>令参战角色依次对一名其他角色使用一张【杀】，直到一名参战角色不如此做。每以此法使用一张【杀】，你可令一名参战角色摸一张牌。（不计入出牌阶段使用杀次数）",
	[":ak_Cangshan"]="你可将装备牌当【杀】或【闪】使用或打出。以此法使用的【杀】无视距离，每以此法使用或打出一张牌时，你摸一张牌，若于回合外，则额外摸一张牌。",
	[":ak_Yuehuang"]="<font color=\"Sky Blue\"><b>萌战技,绝唱,3,出牌阶段限一次;</b></font>令其他参战角色各交给你一张装备牌，然后参战角色各摸X张牌。若如此做，该阶段内你可额外使用X张【杀】（X为以此法交给你的装备牌的数量+1）",
	[":ak_Jingming"]="其他角色的回合开始时，你可弃置一张牌，然后令其不能使用【杀】直到其回合结束；若如此做，回合结束时，你须令其回复1点体力，或其摸两张牌。",
	[":ak_Yingxian"]="<font color=\"Sky Blue\"><b>萌战技,绝唱,3,出牌阶段限一次;</b></font>参战角色各展示一张手牌，然后你将牌堆顶X张牌以任意方式交给参战角色。（X为所有展示手牌的点数和的算术平方根，向上取整）",
	[":ak_Saoshe"]="<font color=\"green\"><b>出牌阶段对每名角色限三次，</b></font>你可指定一名你攻击范围内的其他角色，然后交给其一张牌并视为你对其使用一张【杀】（不计入出牌阶段使用【杀】次数。）当你于出牌阶段内以此法交给其他角色的手牌首次达到两张或更多时，你回复1点体力，或摸两张牌。",
	[":ak_Dikai"]="<font color=\"Sky Blue\"><b>萌战技,绝唱,1,绝唱角色受到伤害后;</b></font>若其参战，你可摸一张牌，然后弃置伤害来源的一张牌并可交给参战角色一张【杀】或装备牌。",
	[":ak_Yingqiang"]="出牌阶段限一次，你可将至多四张花色不同的手牌当一张无距离限制的【杀】使用；若其中牌的花色为：<font color=\"red\">♥</font>，此【杀】对目标角色造成伤害后，你可以令其摸X张牌（X为该角色当前的体力值且至多为5），然后该角色将其武将牌翻面；<font color=\"red\">♦</font>，目标角色需连续使用两张【闪】才能抵消此【杀】；♠，此【杀】造成伤害后，你回复1点体力；♣，此【杀】无视目标角色的防具。",
	[":ak_Shiyan"]="每当你失去一次牌时，你可以将牌堆顶牌置于你的武将牌上，称为“环”。准备阶段开始时，你获得三张“环”，然后将其余的“环”置入弃牌堆。<font color=\"Sky Blue\"><b>萌战技,魔法少女,1,魔法少女受到伤害后;</b></font>你可以令其获得一张“环”，若为红桃牌，其回复1点体力。",
	[":ak_Mingce"]="<font color=\"green\"><b>出牌阶段限一次，</b></font>你可将一张【杀】或装备牌交给一名角色，令该角色选择一项：视为对其攻击范围内的你选择的一名其他角色使用一张雷【杀】，或摸一张牌。",
	[":ak_XZhulou"]="结束阶段开始时，你可摸两张牌，然后选择一项：弃置一张武器牌，或失去1点体力。",
	[":ak_Daoshou"]="每当你的体力值发生变化时，你可选择一项：视为使用一张【铁索连环】，或获得一名其他角色的武器牌。",
	[":ak_Iceberg"]="<font color=\"blue\"><b>锁定技，</b></font>每当你使用（指定目标后）或被使用（成为目标后）一张黑色牌时，你将目标角色的各一张手牌置于你的武将牌上，称为“冰”。若你没有手牌，当其他角色计算与你的距离时，始终+X（X为你的“冰”的数量）。",
	[":ak_Pure"]="出牌阶段限一次，若你手牌多于“冰”，你可将所有手牌交给一名其他角色，若交给其牌的张数大于其当前体力值，视为其使用一张【杀】；若你手牌少于“冰”，你可将所有”冰“交给一名其他角色，若交给其牌的张数大于其当前体力值，视为其使用任意一张非延时锦囊牌。",
	[":ak_PureVS"]="当响应技能【纯粹】时点击。",
	[":ak_Yiren"]="<font color=\"blue\"><b>锁定技，</b></font>准备阶段开始时，若你是已损失的体力值最多的角色或不是体力上限最多的角色，你须选择一项：1.回复1点体力；2.加2点体力上限。",
	[":ak_Lianxin"]="回合结束时，若你于回合内使用的每一张牌的的点数均大于你上一张使用的牌的点数（至少一张），你可以失去1点体力，减1点体力上限，然后进行一个额外的回合。",
}
------------------------------------------------------------------------武将信息区
sgs.LoadTranslationTable{
	["designer:tsukushi"]="OmnisReen",
	["designer:kiriko"]="OmnisReen",
	["designer:itoumakoto"]="OmnisReen",
	["designer:yuuta"]="OmnisReen",
	["designer:odanobuna"]="OmnisReen",
	["designer:keima"]="OmnisReen",
	["designer:ayanami"]="OmnisReen",
	["designer:batora"]="OmnisReen",
	["designer:sheryl"]="昂翼天使; OmnisReen",
	["designer:mao_maoyu"]="昂翼天使; OmnisReen",
	["designer:aoitori"]="OmnisReen",
	["designer:kyouko"]="昂翼天使",
	["designer:diarmuid"]="银龙幽影",
	["designer:ikarishinji"] ="OmnisReen ; 起个ID好烦",
	["designer:redarcher"]="昂翼天使",
	["designer:redo"]="起个ID好烦",
	["designer:runaria"]="昂翼天使",
	["designer:fuwaaika"]="昂翼天使",
	["designer:slsty"]="昂翼天使",
	["designer:rokushikimei"]="起个ID好烦",
	["designer:bernkastel"]="海猫鸣泣之时吧&果然萝卜斩",
	["designer:hibiki"]="OmnisReen",
	["designer:kntsubasa"]="OmnisReen",
	["designer:khntmiku"]="OmnisReen",
	["designer:yukinechris"]="OmnisReen",
	["designer:sagara"]="OmnisReen",
	["designer:toumakazusa"]="昂翼天使",
	["designer:naoeriki"]="OmnisReen",
	["cv:tsukushi"]="田村ゆかり",
	["cv:kiriko"]="松冈祯丞",
	["cv:itoumakoto"]="平川大辅",
	["cv:yuuta"]="福山潤",
	["cv:odanobuna"]="伊藤 かな恵",
	["cv:keima"]="下野紘",
	["cv:ayanami"]="林原 めぐみ",
	["cv:batora"]="小野大辅",
	["cv:sheryl"]="远藤绫 & May'n",
	["cv:mao_maoyu"]="小清水亜美",
	["cv:aoitori"]="福山潤",
	["cv:kyouko"]="野中藍",
	["cv:diarmuid"]="緑川光",
	["cv:ikarishinji"]="緒方 恵美",
	["cv:redarcher"]="諏訪部 順一",
	["cv:redo"]="石川 界人",
	["cv:runaria"]="红野ミア",
	["cv:fuwaaika"]="花澤 香菜",
	["cv:slsty"]="椎名　へきる",
	["cv:rokushikimei"]="無",
	["cv:bernkastel"]="田村ゆかり",
	["cv:hibiki"]="悠木碧",
	["cv:khntmiku"]="井口裕香",
	["cv:kntsubasa"]="水樹奈々",
	["cv:yukinechris"]="高垣彩陽",
	["cv:sagara"]="江口拓也",
	["cv:toumakazusa"]="生天目仁美",
	["cv:naoeriki"]="民安智恵",
	["illustrator:tsukushi"] = "chibi",
	["illustrator:kiriko"] = "まちふか",
	["illustrator:itoumakoto"] = "おこたｎ",
	["illustrator:yuuta"] = "Kyoto Animation",
	["illustrator:odanobuna"] = "みやま零",
	["illustrator:keima"] = "えびさわ",
	["illustrator:ayanami"] = "90i || MC",
	["illustrator:batora"]="水野英多",
	["illustrator:sheryl"]="ち捺 || りゆう",
	["illustrator:mao_maoyu"]="humi",
	["illustrator:aoitori"]="サク",
	["illustrator:kyouko"]="ファルまろ",
	["illustrator:diarmuid"]="羊习习",
	["illustrator:ikarishinji"]="倉鋪",
	["illustrator:redarcher"]="Type Moon || 藤真拓哉 || 比村奇石",
	["illustrator:redo"]="Production I.G.",
	["illustrator:runaria"]="Nitro+",
	["illustrator:fuwaaika"]="ねこてまり",
	["illustrator:slsty"]="燕尾",
	["illustrator:rokushikimei"]="Innocent Grey",
	["illustrator:bernkastel"]="ひさｎ",
	["illustrator:hibiki"]="みなもん",
	["illustrator:kntsubasa"]="h-new",
	["illustrator:yukinechris"]="h-new",
	["illustrator:khntmiku"]="水ようかん",
	["illustrator:sagara"]="Studio五组×MADHOUSE",
}
------------------------------------------------------------------------武将配音区
sgs.LoadTranslationTable{
	["$ak_Tiaojiao1"]="啊！！！“绷”~~~",
	["$ak_Tiaojiao2"]="现在就把你赶出去！再也不让你踏进这个家门半步了！",
	["$ak_Gqset1"]="真有种啊！",
	["$ak_Gqset2"]="（你这！） 砰！",
	["$ak_Shenzhi1"]="我已经看到结局了",
	["$ak_Gonglve1"]="还没穿短裤！",
	["$ak_Gonglve2"]="欢迎~迷途羔羊们。。。",
	["$ak_Blackflame1"]="鉴于你这次的努力和功绩，本漆黑烈焰使在此聚集漆黑之力，为你生成一条独一无二的暗号。识别暗号-刻印！",
	["$weixiao1"]="你为什么而哭呢？",
	["$weixiao2"]="对不起，我不知道该做什么表情好。",
	["$ak_TianmoDefense1"]="谢谢你，猴子。我又被你救了呢~~",
	["$ak_Buwu1"]="我一定要创造一个不会再发生这样战争的世界~对吧！",
	["$ak_Buwu2"]="我眼中所见，是整个世界！",
	["$ak_Chigui1"]="得美浓者的天下~",
	["$ak_Chigui2"]="这种规矩，就由我来打破！",
	["$renzha1"]="我这边很麻烦的啦~",
	["$ak_Renzha1"]="啊~~~ 可恶！",
	["$renzha1"]="才不是",
	["$renzha2"]="世界？",
	["$ak_Gongming1"]="与神明相恋之时，从未想过这样的离别会降临 ",
	["$ak_Gongming2"]="与你相逢 群星闪耀 赐我新生 去爱了才会… 拥有爱才会… ",
	["$ak_Yaojing1"]="（啪！）来 听我的歌吧！",
	["$ak_Yaojing2"]="曾经伫立在世界中心，星球因我而旋转 无意中的喷嚏，便惊起林中蝴蝶乱舞 ",
	["$ak_Boxue1"]="战争的彼端 未曾见过的世界",
	["$ak_Boxue2"]="你好（勇者）",
	["$ak_Bimie1"]="如果不是这样的话，你就能挡下【必灭的黄蔷薇】了",
	["$ak_Pomo1"]="哗~哗~哗~哗~",
	["$ak_Luoshen1"]="超烦~超烦人~~",
	["$ak_Luoshen2"]="真可笑呢~",
	["$ak_Luowang1"]="如同偷窥围裙里的胖次 难以表达（直视= =）",
	["$ak_Luowang2"]="赫~赫~~赫莱森！！！",
	["$ak_Kuangshi1"]="砰！啊~~啊！！！啊！！！！",
	["$ak_Kuangshi2"]="唔啊！！！！！！！！",
	["$ak_Baozou1"]="我变成什么样无所谓 世界变成什么样无所谓。但是绫波。。。 一定要救出来！",
	["$ak_Baozou2"]="把绫波 还我！",
	["$ak_Xinbi1"]="救救我。。。救救我。明日香。",
	["$ak_Xinbi2"]="(不需要。) 所以请温柔一点对我啊。。",
	["$ak_Gongqi1"]="I'm the bone of my sword.",
	["$ak_Touying1"]="Steel is my body,and fire is my blood. d",
	["$ak_Jianyong1"]="Unknown to Death. Nor known to life",
	["$ak_Jianyu1"]="Yet, those hands will never hold anything. ",
	["$ak_Jianzhi1"]="So as I pray, unlimited blade works.  ",
	["$ak_ChamberMove1"]="干翻他！钱伯。",
	["$ak_RedoWake1"]="去死吧！铁皮魂淡！",
	["$ak_JiguangAsk1"]="哗~哗~~哗~~~",
	["$ak_Gaoxiao1"]="全面同意",
	["$ak_Gaokang1"]="可以观测他们想对本机造成伤害，但苦于没有手段，推测他们的文明低下。",
	["$ak_Wenle1"]="哪都行，请带我离开这里",
	["$ak_Yukong1"]="那样的话，就算被我代替了也没啥怨言吧",
	["$ak_Qisi1"]="真狼狈呢",
	["$ak_Poshi1"]="一切都是你的罪过",
	["$ak_Liansuo1"]="天地之间有许多事情是你的睿智无法想象的",
	["$ak_Yinguo1"]="这是怎样一个被诅咒的因果啊",
	["$slyanhuo1"]="能够存活下来的人不是强者也不是智者，而是能够接受并适应变化的人",
	["$zhahu1"]="自作自受",
	["$suipian1"]="这样一边倒的游戏 真无聊",
	["$suipian2"]="又怎样？",
	["$qiji1"]="你可真是记仇呢",
	["$qiji2"]="你爱怎么样就怎么样吧，既然如此那我就认真了。",
	["$ak_Gungnir1"]="Balwisyall Nescell gungnir tron",
	["$ak_Gungnir2"]="届け！",
	["$ak_Gungnir3"]="我明白了。。。",
	["$ak_Synchrogazer1"]="开始吧！S2CA TRIBURST!",
	["$ak_Synchrogazer2"]="Gatrandis babel ziggurat edenal Emustrolronzen fine el baral zizzl Gatrandis babel ziggurat edenal Emustrolronzen fine el zizzl",
	["$ak_Cangshan1"]="那我这边也来真的吧！",
	["$ak_Cangshan2"]="（划破声）",
	["$ak_Yuehuang1"]="利刃风急千花绽",
	["$ak_Yuehuang2"]="（圣咏）",
	["$ak_Dikai1"]="接下来就是我的活儿了！",
	["$ak_Saoshe1"]="二话不说甩起加特林 死亡派对将你送往垃圾箱。",
	["$ak_Saoshe2"]="Combination Arts！",
	["$ak_Jingming1"]="我不想让你再战斗下去了。",
	["$ak_Jingming2"]="据说这件Gear放出的光芒，能照亮新的世界。",
	["$ak_Yingxian1"]="闪光…创始的世界　漆黑…终结的世界",
	["$ak_Yingxian2"]="（圣咏）",
	["$ak_Dikai2"]="（圣咏）",
	["~tsukushi"]="不要。。我不会再放开了。。。",
	["~keima"]="就一步，为现实妥协。。。",
	["~yuuta"]="啊~~~砰！",
	["~ayanami"]="为什么~~",
	["~odanobuna"]="还会。。。再会的吧。",
	["~itoumakoto"]="嘘~~~~~~~~~~",
	["~sheryl"]="但是，我的工作已经结束了。",
	["~mao_maoyu"]="真为这样的自己感到难过。",
	["~kyouko"]="拜托了 神啊 都已经是这样的人生了，至少让我做一次幸福的梦吧。",
	["~aoitori"]="（哦哦哦哦哦哦~~~）",
	["~diarmuid"]="我诅咒圣杯，诅咒你们的愿望成为灾祸。等你们堕入地狱火海之时，就会想起我迪卢木多的愤怒！",
	["~ikarishinji"]="我最差劲了。。",
	["~redo"]="贵官将得到 自由睡眠权 自由饮食权 还能获得 自由生殖权",
	["~redarcher"]="如你所见，你所要面对的是无限之剑，剑击的极限。豁出性命来吧。",
	["~runaria"]="即使世上所有的人都离开了你，我也会和你在一起",
	["~fuwaaika"]="啊，这是多么惊人的事件，我既是被害者，又是侦探，同时还是犯人",
	["~slsty"]="那么就此别过，来世再见",
	["~bernkastel"]="这个游戏永远都不会完结，只是暂时把棋盘盖起来而已。",
	["~hibiki"]="不要！我不要再放开你。",
	["~khntmiku"]="不对，我希望的，才不是这种事情！才不是这种事情！！",
	["~kntsubasa"]="是吧，奏？",
	["~yukinechris"]="永别了",
}
------------------------------------------------------------------------技能动画区
sgs.LoadTranslationTable{
	["ak_Boxue$"] = "image=image/animate/LuaBoxue.png",
	["ak_Yaojing$"] = "image=image/animate/LuaYaojing.png",
	["ak_Renzha$"] = "image=image/animate/LuaRenzha.png",
	["ak_Buwu$"] = "image=image/animate/LuaBuwu.png",
	["weixiao$"] = "image=image/animate/weixiao.png",
	["ak_Gqset$"] = "image=image/animate/LuaGqset.png",
	["ak_Shenzhi$"] = "image=image/animate/LuaShenzhi.png",
	["ak_Blackflame$"] = "image=image/animate/LuaBlackflame.png",
	["ak_Luowang$"] = "image=image/animate/LuaLuowang.png",
	["ak_Bimie$"] = "image=image/animate/LuaBimie.png",
	["ak_Jianzhi$"] = "image=image/animate/LuaJianzhi.png",
	["$ak_RedoWake"] = "anim=skill/LuaRedoWake",
}
----------------------------------------------------------------------------------------------------------